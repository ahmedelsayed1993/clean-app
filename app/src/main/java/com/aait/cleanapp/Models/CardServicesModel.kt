package com.aait.cleanapp.Models

import java.io.Serializable

class CardServicesModel:Serializable {
    var id:Int?=null
    var checked:Int?=null
    var meter:String?=null
    var name:String?= null
}