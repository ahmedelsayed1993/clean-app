package com.aait.cleanapp.Models

import java.io.Serializable

class ProductModel:Serializable {
    var id:Int?=null
    var product_id:Int?=null
    var count:Int?=null
    var name:String?=null
    var image:String?=null
    var subcategory_id:Int?=null

    constructor(id: Int?, product_id: Int?, count: Int?, name: String?, image: String?) {
        this.id = id
        this.product_id = product_id
        this.count = count
        this.name = name
        this.image = image
    }
}