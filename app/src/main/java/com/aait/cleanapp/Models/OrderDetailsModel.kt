package com.aait.cleanapp.Models

import java.io.Serializable

class OrderDetailsModel:Serializable {
    var id:Int?=null
    var username:String?= null
    var image:String?=null
    var lat:String?=null
    var lng:String?=null
    var category:String?=null
    var delegate_id:Int?=null
    var provider_id:Int?=null
    var items:ArrayList<ItemModel>?=null
    var products:ArrayList<ProductsModel>?=null
}