package com.aait.cleanapp.Models

import java.io.Serializable

class CodeResponse:BaseResponse(),Serializable {
    var data:Float?=null
    var total_order:Float?=null
    var discount:String?=null

}