package com.aait.cleanapp.Models

import java.io.Serializable

class ListModel :Serializable{
    var id:Int?=null
    var name:String?=null
    var image:String?=null
    var category_key:String?=null

    constructor(id: Int?, name: String?) {
        this.id = id
        this.name = name
    }
}