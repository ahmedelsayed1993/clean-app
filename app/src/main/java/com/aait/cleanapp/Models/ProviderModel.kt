package com.aait.cleanapp.Models

import java.io.Serializable

class ProviderModel:Serializable {

    var id:Int?=null
    var name:String?=null
    var address:String?=null
    var distance:Double?=null
    var image:String?=null
}