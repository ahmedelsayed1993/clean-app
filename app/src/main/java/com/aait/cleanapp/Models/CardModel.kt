package com.aait.cleanapp.Models

import java.io.Serializable

class CardModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var provider_id:Int?=null
    var address:String?=null
    var category:String?=null
    var category_id:Int?=null
    var category_key:String?=null
    var image:String?=null
}