package com.aait.cleanapp.Models

import java.io.Serializable

class SearchModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var distance:Float?=null
    var category_id:Int?=null
    var category_key:String?=null
    var address:String?=null
    var image:String?=null
}