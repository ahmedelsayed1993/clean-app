package com.aait.cleanapp.Models

import java.io.Serializable

class ProviderResponse:BaseResponse(),Serializable {
    var banners:ArrayList<BannersModel>?=null
    var data:ArrayList<ProviderModel>?=null
}