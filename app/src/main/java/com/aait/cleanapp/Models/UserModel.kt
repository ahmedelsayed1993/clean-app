package com.aait.cleanapp.Models

import java.io.Serializable

class UserModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var phone:String?=null
    var code:String?=null
    var user_type:String?=null
    var status:String?=null
    var device_id:String?=null
    var device_type:String?=null
    var avatar:String?=null
    var date:String?=null
    var lat:String?=null
    var lng:String?=null
    var national_address:String?=null
    var star_id:String?=null
    var star_image:String?=null
    var star_name:String?=null
}