package com.aait.cleanapp.Models

import java.io.Serializable

class Order:Serializable {
    var subcategory_id:Int?=null
    var product_id:String?=null
    var count:Int?=null

    constructor(subcategory_id: Int?, product_id: String?, count: Int?) {
        this.subcategory_id = subcategory_id
        this.product_id = product_id
        this.count = count
    }
}