package com.aait.cleanapp.Models

import java.io.Serializable

class ServicesModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var checked:Int?=null
    var price:String?=null
    var count:String?=null

    constructor(id: Int?, name: String?, checked: Int?, price: String?) {
        this.id = id
        this.name = name
        this.checked = checked
        this.price = price
    }
}