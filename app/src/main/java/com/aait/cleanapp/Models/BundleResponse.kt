package com.aait.cleanapp.Models

import java.io.Serializable

class BundleResponse:BaseResponse(),Serializable {
    var data:ArrayList<BundleModel>?=null
}