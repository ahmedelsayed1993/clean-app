package com.aait.cleanapp.Models

import java.io.Serializable

class CardDetailsOrder:Serializable {
    var id:Int?=null
    var notes:String?=null
    var lat:String?=null
    var lng:String?=null
    var address:String?=null
    var date:String?=null
    var time_from:String?=null
    var time_to:String?=null
    var payment:String?=null
    var account_name:String?=null
    var another_person:String?=null
    var phone:String?=null
}