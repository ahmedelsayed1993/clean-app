package com.aait.cleanapp.Models

import java.io.Serializable

class CardDetailsOrderResponse:BaseResponse(),Serializable {
    var data:ArrayList<CardOrderModel>?=null
    var services:ArrayList<CardServicesModel>?=null
    var details_order:CardDetailsOrder?=null
    var notes:String?=null
    var delivery:String?= null
    var tax:String?=null
    var total_tax:String?=null
    var total:String?=null
}