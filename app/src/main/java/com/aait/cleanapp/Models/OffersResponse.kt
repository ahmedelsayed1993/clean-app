package com.aait.cleanapp.Models

import java.io.Serializable

class OffersResponse:BaseResponse(),Serializable {
    var data:ArrayList<OfferModel>?=null
}