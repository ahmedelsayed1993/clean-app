package com.aait.cleanapp.Models

import java.io.Serializable

class ServicesResponse:BaseResponse(),Serializable {
    var data:ArrayList<ServicesModel>?=null
}