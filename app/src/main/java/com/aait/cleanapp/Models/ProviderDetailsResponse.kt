package com.aait.cleanapp.Models

import java.io.Serializable

class ProviderDetailsResponse:BaseResponse(),Serializable {
    var data:ProviderDetailsModel?=null
}