package com.aait.cleanapp.Models

import java.io.Serializable

class SubCategoriesModel:Serializable {
    var id:Int?=null
    var subcategory_id:Int?=null
    var subcategory_name:String?=null
    var subcategory_image:String?=null
    var products:ArrayList<ProductModel>?=null
}