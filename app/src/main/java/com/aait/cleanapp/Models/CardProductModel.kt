package com.aait.cleanapp.Models

import java.io.Serializable

class CardProductModel:Serializable {
    var id:Int?=null
    var name:String?=null
    var count:String?=null
    var image:String?=null
}