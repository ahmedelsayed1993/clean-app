package com.aait.cleanapp.Models

import java.io.Serializable

class ProviderDetailsModel:Serializable {
    var images:ArrayList<BannersModel>?=null
    var id:Int?=null
    var name:String?= null
    var address:String?=null
    var rate:Float?=null
    var share_link:String?=null
    var description:String?= null
}