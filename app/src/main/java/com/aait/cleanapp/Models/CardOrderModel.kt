package com.aait.cleanapp.Models

import java.io.Serializable

class CardOrderModel:Serializable {
    var id:Int?=null
    var subcategory:Int?=null
    var subcategory_name:String?=null
    var subcategory_image:String?=null
    var products:ArrayList<CardProductModel>?=null
}