package com.aait.cleanapp.Models

import java.io.Serializable

class SubCategoriesResponse:BaseResponse(),Serializable {
    var data:ArrayList<SubCategoriesModel>?=null
    var services:ArrayList<ServicesModel>?=null
}