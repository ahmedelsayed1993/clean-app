package com.aait.cleanapp.Models

import java.io.Serializable

class SearchResponse:BaseResponse(),Serializable {
    var data:ArrayList<SearchModel>?=null
}