package com.aait.cleanapp.Models

import java.io.Serializable

class OrderModel:Serializable {
    var id:Int?=null
    var provider_id:Int?=null
    var name:String?=null
    var address:String?=null
    var image:String?=null
    var status:String?=null
}