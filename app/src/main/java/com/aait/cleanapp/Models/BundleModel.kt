package com.aait.cleanapp.Models

import java.io.Serializable

class BundleModel:Serializable {
    var id:Int?=null
    var content:String?=null
    var price:Int?=null
}