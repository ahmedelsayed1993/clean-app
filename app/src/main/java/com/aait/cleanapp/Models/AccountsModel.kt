package com.aait.cleanapp.Models

import java.io.Serializable

class AccountsModel:Serializable {
    var id:Int?=null
    var bank_name:String?=null
    var account_name:String?=null
    var account_number:String?=null
    var iban_number:String?=null
}