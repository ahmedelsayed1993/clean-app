package com.aait.cleanapp.Base

import android.content.Context
import android.util.Log

import androidx.recyclerview.widget.RecyclerView
import com.aait.cleanapp.Listeners.OnItemClickListener
import com.aait.cleanapp.Perefences.LanguagePrefManager


import com.aait.cleanapp.Perefences.SharedPrefManager

/**
 * is a base class to extend from it the recyclerview adapter
 */
abstract class ParentRecyclerAdapter<Item> : RecyclerView.Adapter<ParentRecyclerViewHolder> {


    protected lateinit var mcontext: Context
    protected var id:String = ""
    protected var name:String = ""

    internal lateinit var data: MutableList<Item>
    protected lateinit var onItemClickListener:OnItemClickListener

    protected var layoutId: Int = 0

    protected var isLoadingAdded = false

    protected var retryPageLoad = false




    protected lateinit var mSharedPrefManager: SharedPrefManager
    lateinit var mLanguagePrefManager: LanguagePrefManager


    constructor(context: Context) {
        this.mcontext = context
        mSharedPrefManager = SharedPrefManager(context)
        mLanguagePrefManager = LanguagePrefManager(context)

    }

    constructor(context: Context, data: MutableList<Item>) {
        this.mcontext = context
        this.data = data
        mSharedPrefManager = SharedPrefManager(context)
        mLanguagePrefManager = LanguagePrefManager(context)

    }
    constructor(context: Context, data: MutableList<Item>,id:String) {
        this.mcontext = context
        this.data = data
        this.id = id
        mSharedPrefManager = SharedPrefManager(context)
        mLanguagePrefManager = LanguagePrefManager(context)

    }
    constructor(context: Context, data: MutableList<Item>,id:String,name:String) {
        this.mcontext = context
        this.data = data
        this.id = id
        this.name = name
        mSharedPrefManager = SharedPrefManager(context)
        mLanguagePrefManager = LanguagePrefManager(context)

    }

    constructor(context: Context, data: MutableList<Item>, layoutId: Int) {
        this.mcontext = context
        this.data = data
        this.layoutId = layoutId
        mSharedPrefManager = SharedPrefManager(context)
        mLanguagePrefManager = LanguagePrefManager(context)
    }

    constructor()


    override fun getItemCount(): Int {
        return data.size
    }




    fun InsertAll(items: List<Item>) {
        data.addAll(items)
        notifyDataSetChanged()
    }

    fun Insert(position: Int, item: Item) {
        data.add(position, item)
        Log.e("Test_Test", position.toString() + "")
        notifyDataSetChanged()
    }

    fun Delete(position: Int) {
        data.removeAt(position)
        notifyDataSetChanged()
    }

    fun update(position: Int, item: Item) {
        data.removeAt(position)
        data.add(position, item)
        notifyDataSetChanged()
    }

    fun updateAll(items: List<Item>) {
        data.clear()
        data.addAll(items)
        notifyDataSetChanged()
    }

    fun getData(): List<Item> {
        return data
    }
    internal fun setOnItemClickListener(itemClickListener: OnItemClickListener){
        this.onItemClickListener = itemClickListener

    }

//    fun addFooterProgress() {
//        val add = this.data.add(null)
//        notifyItemInserted(data.size - 1)
//    }

    fun removeFooterProgress() {
        data.removeAt(data.size - 1)
        notifyItemRemoved(data.size)
        Log.e("footer", "gone")
    }


    fun addLoadingFooter(item: Item) {
        isLoadingAdded = true
        data.add(item)
        notifyItemInserted(data.size - 1)
    }

    fun removeLoadingFooter() {
        isLoadingAdded = false
        val position = data.size - 1
        data.removeAt(position)
        notifyItemRemoved(position)
    }
}
