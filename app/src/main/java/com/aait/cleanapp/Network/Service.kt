package com.aait.cleanapp.Network

import com.aait.cleanapp.Models.*
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.Query

interface Service {

    @POST("cities")
    fun cities(@Query("lang") lang:String):Call<ListResponse>

    @POST("about")
    fun About(@Query("lang") lang:String):Call<AboutAppResponse>

    @POST("terms")
    fun Terms(@Query("lang") lang:String):Call<AboutAppResponse>

    @POST("contact-us")
    fun contact(@Query("lang") lang:String,
                @Query("name") name:String?,
                @Query("email") email:String?,
                @Query("message") message:String?):Call<ContactResponse>



    @POST("check-code")
    fun CheckCode(@Query("user_id") user_id:Int,
                  @Query("code") code:String,
                  @Query("lang") lang:String):Call<UserResponse>
    @POST("resend-code")
    fun ReSend(@Query("user_id") user_id:Int):Call<AboutAppResponse>

    @POST("sign-in")
    fun SignIn(@Query("phone") phone:String,
               @Query("password") password:String,
               @Query("device_id") device_id:String,
               @Query("device_type") device_type:String,
               @Query("lang") lang:String):Call<UserResponse>

    @POST("categories")
    fun categories(@Query("lang") lang: String,
                   @Query("user_id") user_id: Int):Call<ListResponse>

    @POST("packages")
    fun pakages(@Query("lang") lang: String):Call<BundleResponse>
    @POST("balance")
    fun balance(@Query("lang") lang:String,
                @Query("user_id") user_id: Int):Call<AboutAppResponse>
    @POST("sign-up-user")
    fun SignUp(@Query("phone") phone:String,
               @Query("name") name:String,
               @Query("lat") lat:String,
               @Query("lng") lng:String,
               @Query("national_address") national_address:String,
               @Query("password") password:String,
               @Query("device_id") device_id:String,
               @Query("device_type") device_type:String,
               @Query("lang") lang: String):Call<UserResponse>
    @POST("edit-profile")
    fun getProfile(@Query("lang") lang:String,
                   @Query("user_id") user_id: Int):Call<UserResponse>
    @Multipart
    @POST("edit-profile")
    fun updateAvatar(@Query("lang") lang:String,
                   @Query("user_id") user_id: Int,
                     @Part avatar:MultipartBody.Part):Call<UserResponse>

    @POST("edit-profile")
    fun updateprofile(@Query("lang") lang:String,
                     @Query("user_id") user_id: Int,
                     @Query("lat") lat:String,
                      @Query("lng") lng:String,
                      @Query("national_address") national_address:String,
                      @Query("name") name:String,
                      @Query("phone") phone:String):Call<UserResponse>
    @POST("reset-password")
    fun resetPassword(@Query("lang") lang:String,
                      @Query("user_id") user_id: Int,
                      @Query("current_password") current_password:String,
                      @Query("password") password:String):Call<BaseResponse>

    @POST("log-out")
    fun logOut(@Query("lang") lang: String,
               @Query("user_id") user_id: Int,
               @Query("device_type") device_type:String,
               @Query("device_id") device_id:String):Call<AboutAppResponse>
    @POST("bank-accounts")
    fun getAccounts(@Query("lang") lang: String,
                    @Query("user_id") user_id: Int):Call<AccountsResponse>
    @Multipart
    @POST("bank-transfer")
    fun BankTransfer(@Query("user_id") user_id: Int,
                     @Query("lang") lang: String,
                     @Query("name") name:String,
                     @Query("bank_name") bank_name:String,
                     @Query("amount") amount:String,
                     @Part image:MultipartBody.Part):Call<AboutAppResponse>
    @POST("get-providers")
    fun getProviders (@Query("lang") lang:String,
         @Query("user_id") user_id:Int,
         @Query("category_id") category_id:Int,
         @Query("lat") lat:String,
         @Query("lng") lng:String):Call<ProviderResponse>

    @POST("provider-details")
    fun getProvider(@Query("lang") lang: String,
                    @Query("provider_id") provider_id:Int):Call<ProviderDetailsResponse>
    @POST("provider-subcategories")
    fun SubCats(@Query("lang") lang:String,
                @Query("user_id") user_id: Int,
                @Query("provider_id") provider_id:Int):Call<SubCategoriesResponse>

    @POST("product-services")
    fun ProductServices(@Query("lang") lang:String,
                        @Query("user_id") user_id:Int,
                        @Query("product_id") product_id:Int):Call<ServicesResponse>

    @POST("add-order")
    fun AddOrder(@Query("lang") lang:String,
                 @Query("user_id") user_id:Int,
                 @Query("provider_id") provider_id:Int,
                 @Query("order") order:String,
                 @Query("services") services:String?,
                 @Query("notes") notes:String,
                 @Query("account_name") account_name:String,
                 @Query("another_person") another_person:String,
                 @Query("phone") phone:String,
                 @Query("lat") lat:String,
                 @Query("lng") lng:String,
                 @Query("address") address:String,
                 @Query("date") date:String,
                 @Query("time_from") time_from:String,
                 @Query("time_to") time_to:String,
                 @Query("is_meter") is_meter:Int):Call<AddOrderResponse>

    @POST("update-order")
    fun UpdateOrder(@Query("lang") lang:String,
                    @Query("user_id") user_id:Int,
                    @Query("order_id") order_id:Int,
                    @Query("order") order:String,
                    @Query("services") services:String?,
                    @Query("notes") notes:String,
                    @Query("account_name") account_name:String,
                    @Query("another_person") another_person:String,
                    @Query("phone") phone:String,
                    @Query("lat") lat:String,
                    @Query("lng") lng:String,
                    @Query("address") address:String,
                    @Query("date") date:String,
                    @Query("time_from") time_from:String,
                    @Query("time_to") time_to:String,
                    @Query("is_meter") is_meter:Int):Call<AddOrderResponse>
    @POST("invoice-details")
    fun getInvoice(@Query("lang") lang: String,
                   @Query("user_id") user_id: Int,
                   @Query("order_id") order_id: Int):Call<InvoiceResponse>

    @POST("order-payment")
    fun Pay(@Query("lang") lang:String,
            @Query("user_id") user_id:Int,
            @Query("order_id") order_id:Int,
            @Query("payment") payment:String):Call<AboutAppResponse>
    @POST("carts")
    fun Carts(@Query("lang") lang:String,
              @Query("user_id") user_id:Int):Call<CardResponse>

    @POST("delete-order-cart")
    fun deleteCard(@Query("lang") lang:String,
                   @Query("user_id") user_id:Int,
                   @Query("order_id") order_id:Int):Call<AboutAppResponse>
    @POST("details-order-cart")
    fun getCardDetails(@Query("lang") lang:String,
                       @Query("user_id") user_id: Int,
                       @Query("order_id") order_id:Int):Call<CardDetailsOrderResponse>

    @POST("add-cart")
    fun AddToCard(@Query("lang") lang:String,
                  @Query("user_id") user_id: Int,
                  @Query("provider_id") provider_id: Int,
                  @Query("order") order:String,
                  @Query("services") services:String?,
                  @Query("is_meter") is_meter:Int):Call<AddOrderResponse>

    @POST("user-orders")
    fun orders(@Query("lang") lang: String,
               @Query("user_id") user_id: Int,
               @Query("action") action:String):Call<OrderResponse>

    @POST("details-order")
    fun OrderDetails(@Query("lang") lang: String,
                     @Query("order_id") order_id:Int,
                     @Query("provider_id") provider_id:Int,
                     @Query("action") action:String?,
                     @Query("user_type") user_type:String):Call<OrderDetailsResponse>

    @POST("track-order-user")
    fun Track(@Query("lang") lang:String,
              @Query("user_id") user_id: Int,
              @Query("order_id") order_id:Int):Call<AboutAppResponse>
    @POST("unread-notifications")
    fun UnRead(@Query("user_id") user_id: Int):Call<SwitchNotification>
    @POST("delete-notification")
    fun delete(@Query("lang") lang: String,
               @Query("user_id") user_id:Int,
               @Query("notification_id") notification_id:Int):Call<AboutAppResponse>
    @POST("switch-notification")
    fun switch(@Query("user_id") user_id:Int,
               @Query("switch") switch:Int?):Call<SwitchNotification>
    @POST("forget-password")
    fun ForGot(@Query("phone") phone:String,
               @Query("lang") lang:String):Call<UserResponse>

    @POST("update-password")
    fun NewPass(@Query("user_id") user_id: Int,
                @Query("password") password:String,
                @Query("code") code:String,
                @Query("lang") lang: String):Call<UserResponse>
    @POST("notifications")
    fun Notification(@Query("lang") lang: String,
                     @Query("user_id") user_id:Int):Call<NotificationResponse>
    @POST("offers")
    fun Offers(@Query("lang") lang: String,
               @Query("user_id") user_id: Int):Call<OffersResponse>
    @POST("rate")
    fun Rate(@Query("lang") lang:String,
             @Query("user_id") user_id:Int,
             @Query("provider_id") provider_id:Int,
             @Query("delegate_id")delegate_id:Int,
             @Query("provider_rate") provider_rate:Int,
             @Query("delegate_rate") delegate_rate:Int,
             @Query("provider_notes") provider_notes:String?,
             @Query("delegate_notes") delegate_notes:String?,
             @Query("order_id") order_id:Int):Call<AboutAppResponse>

    @POST("search")
    fun Search(@Query("lang") lang: String,
               @Query("text") text:String,
               @Query("lat") lat:String,
               @Query("lng") lng:String):Call<SearchResponse>

    @POST("filter")
    fun Filter(@Query("lang") lang: String,
               @Query("user_id") user_id: Int,
               @Query("category_id") category_id:Int,
               @Query("rate") rate:Int?,
               @Query("lat") lat: String,
               @Query("lng") lng:String):Call<ProviderResponse>

    @POST("discount-code")
    fun Discount(@Query("lang") lang:String,
                 @Query("user_id") user_id:Int,
                 @Query("order_id") order_id:Int,
                 @Query("discount_code") discount_code:String,
                 @Query("confirm")confirm:Int?):Call<CodeResponse>

    @POST("unread-notifications")
    fun Count(@Query("user_id") user_id: Int):Call<CountResponse>


}