package com.aait.cleanapp.UI.Activities

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Build
import android.util.Log
import android.widget.*
import androidx.annotation.RequiresApi
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.Client
import com.aait.cleanapp.Models.AddOrderResponse
import com.aait.cleanapp.Models.BaseResponse
import com.aait.cleanapp.Models.Order
import com.aait.cleanapp.Network.Service
import com.aait.cleanapp.R
import com.aait.cleanapp.Uitls.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*
import kotlin.collections.ArrayList

class RequestOrderActivity :Parent_Activity(){
    override val layoutResource: Int
        get() = R.layout.activity_request_order
    lateinit var current_location:TextView
    lateinit var location:ToggleButton
    lateinit var another_location:TextView
    lateinit var name:EditText
    lateinit var another_name:EditText
    lateinit var phone:EditText
    lateinit var from:TextView
    lateinit var to:TextView
    lateinit var date:TextView
    lateinit var notes:EditText
    lateinit var confirm:Button
    lateinit var back:ImageView
    var starting = 0
    var ending = 0
    var address = ""
    var lat = ""
    var lng = ""
    var id = 0
    var services = ""
    var products = ArrayList<Order>()
    var category = ""
    var unix = 0L
    var unix1 = 0L
    @RequiresApi(Build.VERSION_CODES.O)
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        services = intent.getStringExtra("services")
        products = intent.getSerializableExtra("products") as ArrayList<Order>
        category = intent.getStringExtra("category")
        current_location =findViewById(R.id.current_location)
        location = findViewById(R.id.location)
        another_location = findViewById(R.id.another_location)
        name = findViewById(R.id.name)
        another_name = findViewById(R.id.another_name)
        phone = findViewById(R.id.phone)
        from = findViewById(R.id.from)
        to = findViewById(R.id.to)
        date = findViewById(R.id.date)
        notes = findViewById(R.id.notes)
        confirm = findViewById(R.id.confirm)
        back = findViewById(R.id.back)
        back.setOnClickListener { onBackPressed()
        finish()}
        val rightNow = Calendar.getInstance()
        var hourByDay = rightNow.get(Calendar.HOUR_OF_DAY)
        Log.e("hour",hourByDay.toString())
        address = mSharedPrefManager.userData.national_address!!
        lat = mSharedPrefManager.userData.lat!!
        lng = mSharedPrefManager.userData.lng!!
        name.setText(mSharedPrefManager.userData.name)
        phone.setText(mSharedPrefManager.userData.phone)
        current_location.text = mSharedPrefManager.userData.national_address
        another_location.setOnClickListener { startActivityForResult(Intent(this@RequestOrderActivity,LocationActivity::class.java),1)  }
        from.setOnClickListener {
            val myCalender = Calendar.getInstance()
            val hour = myCalender.get(Calendar.HOUR_OF_DAY)
            val minute = myCalender.get(Calendar.MINUTE)


            val myTimeListener =
                TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                    if (view.isShown) {
                        myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay)
                        myCalender.set(Calendar.MINUTE, minute)
                        starting = hourOfDay
                        Log.e("start",starting.toString())
                        Log.e("time",LocalDateTime.now().format(DateTimeFormatter.ofLocalizedTime(
                            FormatStyle.SHORT)))
                        var hour = hourOfDay
                        var am_pm = ""
                        // AM_PM decider logic
                        when {
                            hour == 0 -> {
                                hour += 12
                                am_pm = "AM"
                            }
                            hour == 12 -> am_pm = "PM"
                            hour > 12 -> {
                                hour -= 12
                                am_pm = "PM"
                            }
                            else -> am_pm = "AM"
                        }

                        val hours = if (hour < 10) "0" + hour else hour
                        from.text = " $hours $am_pm"

                    }
                }
            val timePickerDialog = TimePickerDialog(
                this,
                android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                myTimeListener,
                hour,
                0,
                false
            )
            timePickerDialog.setTitle(getString(R.string.from))
            timePickerDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            timePickerDialog.show()
        }
        to.setOnClickListener {
            val myCalender = Calendar.getInstance()
            val hour = myCalender.get(Calendar.HOUR_OF_DAY)
            val minute = myCalender.get(Calendar.MINUTE)


            val myTimeListener =
                TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                    if (view.isShown) {
                        myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay)
                        myCalender.set(Calendar.MINUTE, minute)
                        ending = hourOfDay
                        var hour = hourOfDay
                        var am_pm = ""
                        // AM_PM decider logic
                        when {
                            hour == 0 -> {
                                hour += 12
                                am_pm = "AM"
                            }
                            hour == 12 -> am_pm = "PM"
                            hour > 12 -> {
                                hour -= 12
                                am_pm = "PM"
                            }
                            else -> am_pm = "AM"
                        }

                        val hours = if (hour < 10) "0" + hour else hour
                        to.text = " $hours $am_pm"

                    }
                }
            val timePickerDialog = TimePickerDialog(
                this,
                android.R.style.Theme_Holo_Light_Dialog_NoActionBar,
                myTimeListener,
                hour,
                0,
                false
            )
            timePickerDialog.setTitle(getString(R.string.to))

            timePickerDialog.window!!.setBackgroundDrawableResource(android.R.color.transparent)
            timePickerDialog.show()
        }

        date.setOnClickListener {
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)




            val dpd = DatePickerDialog(mContext, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                // Display Selected date in textbox
                var m = monthOfYear+1
                if(dayOfMonth<10) {
                    if(m<10) {
                        date.setText("0" + dayOfMonth + "/0" + m + "/" + year)
                    }else{
                        date.setText("0" + dayOfMonth + "/" + m + "/" + year)
                    }
                }else{
                    if(m<10) {
                        date.setText("" + dayOfMonth + "/0" + m + "/" + year)
                    }else{
                        date.setText("" + dayOfMonth + "/" + m + "/" + year)
                    }
                }
                var c1 = Calendar.getInstance()

                val sdf = SimpleDateFormat("dd/mm/yyyy")
                var d = sdf.parse(date.text.toString())
                val current = LocalDateTime.now()
                val formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy")
                var answer: String =  current.format(formatter)
                var d1 = sdf.parse(answer)
                c1.time = d
                // val l = LocalDate.parse(LocalDateTime.now().toString(), DateTimeFormatter.ofPattern("dd/MM/yyyy"))
                unix1 = d1.toInstant().epochSecond
                unix = d.toInstant().epochSecond
                Log.e("daaa",unix.toString())
                Log.e("current",unix1.toString())

            }, year, month, day)
            dpd.datePicker.minDate = System.currentTimeMillis() - 1000

            dpd.show()
        }


        location.setOnClickListener {
            if (location.isChecked){

                current_location.text = mSharedPrefManager.userData.national_address
            }else{
                current_location.text = ""
            }
        }

        confirm.setOnClickListener {
             if (location.isChecked){
                 if (CommonUtil.checkEditError(name,getString(R.string.writing_name))||

                         CommonUtil.checkEditError(phone,getString(R.string.writing_number))||
                         CommonUtil.checkTextError(to,getString(R.string.determine_time))||
                         CommonUtil.checkTextError(from,getString(R.string.determine_time))||
                         CommonUtil.checkTextError(date,getString(R.string.determine_date))){
                     return@setOnClickListener
                 }else{
                     if (unix<=unix1) {
                         if (starting <= hourByDay) {
                             CommonUtil.makeToast(mContext, getString(R.string.start_time_greater))
                         } else {
                             if (ending <= starting) {
                                 CommonUtil.makeToast(mContext, getString(R.string.starting))
                             } else {

                                 AddOrder()
                             }
                         }
                     }else{
                         if (ending <= starting) {
                             CommonUtil.makeToast(mContext, getString(R.string.starting))
                         } else {

                             AddOrder()
                         }
                     }
                 }
             }else{
                 if (CommonUtil.checkTextError(another_location,getString(R.string.another_location))||
                     CommonUtil.checkEditError(name,getString(R.string.writing_name))||
                     CommonUtil.checkEditError(phone,getString(R.string.writing_number))||
                     CommonUtil.checkTextError(to,getString(R.string.determine_time))||
                     CommonUtil.checkTextError(from,getString(R.string.determine_time))||
                     CommonUtil.checkTextError(date,getString(R.string.determine_date))){
                     return@setOnClickListener
                 }else{
                     if (unix<=unix1) {
                         if (starting <= hourByDay) {
                             CommonUtil.makeToast(mContext, getString(R.string.start_time_greater))
                         } else {
                             if (ending <= starting) {
                                 CommonUtil.makeToast(mContext, getString(R.string.starting))
                             } else {

                                 AddOrder()
                             }
                         }
                     }else{
                         if (ending <= starting) {
                             CommonUtil.makeToast(mContext, getString(R.string.starting))
                         } else {

                             AddOrder()
                         }
                     }
                 }
             }

             }


    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.e("address",data?.getStringExtra("result").toString())
        if (data?.getStringExtra("result")!=null) {
            address = data?.getStringExtra("result").toString()
            lat = data?.getStringExtra("lat").toString()
            lng = data?.getStringExtra("lng").toString()
            another_location.text = address
            location.isChecked =false
            current_location.text = ""
        }else{
            address = mSharedPrefManager.userData.national_address!!
            lat = mSharedPrefManager.userData.lat!!
            lng = mSharedPrefManager.userData.lng!!
            another_location.text = ""
            location.isChecked = true
            current_location.text = address
        }
    }

    fun AddOrder(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.AddOrder(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,
            id,Gson().toJson(products),services,notes.text.toString(),name.text.toString(),another_name.text.toString()
        ,phone.text.toString(),lat,lng,address,date.text.toString(),from.text.toString(),to.text.toString(),0)
            ?.enqueue(object : Callback<AddOrderResponse> {
                override fun onFailure(call: Call<AddOrderResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<AddOrderResponse>,
                    response: Response<AddOrderResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            val intent = Intent(this@RequestOrderActivity,InvoiceActivity::class.java)
                            intent.putExtra("id",response.body()?.data)
                            intent.putExtra("category",category)
                            startActivity(intent)


                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }
}