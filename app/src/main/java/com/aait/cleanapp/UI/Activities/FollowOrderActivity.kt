package com.aait.cleanapp.UI.Activities

import android.content.Intent
import android.os.Build
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import androidx.annotation.RequiresApi
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.Client
import com.aait.cleanapp.Models.AboutAppResponse
import com.aait.cleanapp.Network.Service
import com.aait.cleanapp.R
import com.aait.cleanapp.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FollowOrderActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_follow_order
    lateinit var one: ImageView
    lateinit var two: ImageView
    lateinit var three: ImageView
    lateinit var four:ImageView
    lateinit var back_to_main: Button
    lateinit var back: ImageView
    var id = 0

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        one = findViewById(R.id.one)
        two = findViewById(R.id.two)
        three = findViewById(R.id.three)
        four = findViewById(R.id.four)
        back_to_main = findViewById(R.id.back_to_main)
        back = findViewById(R.id.back)

        back.setOnClickListener { onBackPressed()
            finish()
        }
        back_to_main.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
            finish()}
        FollowOrder()
    }
    fun FollowOrder(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Track(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,
            id)?.enqueue(object : Callback<AboutAppResponse> {
            override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
            override fun onResponse(
                call: Call<AboutAppResponse>,
                response: Response<AboutAppResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        Log.e("response",response.body()?.data!!)
                        if (response.body()?.data.equals("accepted")){

                            back_to_main.visibility = View.GONE
                            one.setImageResource(R.mipmap.right)
                            one.background = mContext.getDrawable(R.drawable.yellow_circle)
                            two.setImageResource(R.mipmap.right)
                            two.background = mContext.getDrawable(R.drawable.white_circle)
                            three.setImageResource(R.mipmap.right)
                            three.background = mContext.getDrawable(R.drawable.white_circle)
                            four.setImageResource(R.mipmap.right)
                            four.background = mContext.getDrawable(R.drawable.white_circle)

                        }else if (response.body()?.data.equals("current")){

                            back_to_main.visibility = View.GONE
                            one.setImageResource(R.mipmap.right)
                            one.background = mContext.getDrawable(R.drawable.white_circle)
                            two.setImageResource(R.mipmap.right)
                            two.background = mContext.getDrawable(R.drawable.white_circle)
                            three.setImageResource(R.mipmap.right)
                            three.background = mContext.getDrawable(R.drawable.white_circle)
                            four.setImageResource(R.mipmap.right)
                            four.background = mContext.getDrawable(R.drawable.white_circle)

                        }
                        else if (response.body()?.data.equals("received_order_client")||response.body()?.data.equals("work_underway")){
                            back_to_main.visibility = View.GONE
                            one.setImageResource(R.mipmap.right)
                            one.background = mContext.getDrawable(R.drawable.yellow_circle)
                            two.setImageResource(R.mipmap.right)
                            two.background = mContext.getDrawable(R.drawable.yellow_circle)
                            three.setImageResource(R.mipmap.right)
                            three.background = mContext.getDrawable(R.drawable.white_circle)
                            four.setImageResource(R.mipmap.right)
                            four.background = mContext.getDrawable(R.drawable.white_circle)

                        }else if (response.body()?.data.equals("finished_order_provider")||response.body()?.data.equals("received_order_delegate")){
                            back_to_main.visibility = View.GONE
                            one.setImageResource(R.mipmap.right)
                            one.background = mContext.getDrawable(R.drawable.yellow_circle)
                            two.setImageResource(R.mipmap.right)
                            two.background = mContext.getDrawable(R.drawable.yellow_circle)
                            three.setImageResource(R.mipmap.right)
                            three.background = mContext.getDrawable(R.drawable.yellow_circle)
                            four.setImageResource(R.mipmap.right)
                            four.background = mContext.getDrawable(R.drawable.white_circle)

                        }else if (response.body()?.data.equals("completed")){
                            back_to_main.visibility = View.VISIBLE
                            one.setImageResource(R.mipmap.right)
                            one.background = mContext.getDrawable(R.drawable.yellow_circle)
                            two.setImageResource(R.mipmap.right)
                            two.background = mContext.getDrawable(R.drawable.yellow_circle)
                            three.setImageResource(R.mipmap.right)
                            three.background = mContext.getDrawable(R.drawable.yellow_circle)
                            four.setImageResource(R.mipmap.right)
                            four.background = mContext.getDrawable(R.drawable.yellow_circle)
                        }

                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
}