package com.aait.cleanapp.UI.Activities

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.Client
import com.aait.cleanapp.GPS.GPSTracker
import com.aait.cleanapp.GPS.GpsTrakerListener
import com.aait.cleanapp.Models.OrderDetailsModel
import com.aait.cleanapp.Models.OrderDetailsResponse
import com.aait.cleanapp.Models.ProductsModel
import com.aait.cleanapp.Network.Service
import com.aait.cleanapp.R
import com.aait.cleanapp.UI.Adapters.CountAdapter
import com.aait.cleanapp.Uitls.CommonUtil
import com.aait.cleanapp.Uitls.DialogUtil
import com.aait.cleanapp.Uitls.PermissionUtils
import com.bumptech.glide.Glide
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class PreviousActivity : Parent_Activity(), GpsTrakerListener {
    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }

    override val layoutResource: Int
        get() = R.layout.activity_previous_order
    internal lateinit var googleMap: GoogleMap
    internal lateinit var myMarker: Marker
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""


    private var mAlertDialog: AlertDialog? = null
    var id = 0
    lateinit var Image: ImageView

    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var name: TextView
    lateinit var address: TextView
    lateinit var order_num: TextView
    lateinit var location: TextView
    lateinit var pay: TextView
    lateinit var service: TextView
    lateinit var service_type: TextView
    lateinit var notes: TextView
    var lat = ""
    var lng = ""
    lateinit var follow: Button
    lateinit var rate:Button
    lateinit var products: RecyclerView
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var countAdapter: CountAdapter
    var productsModels = ArrayList<ProductsModel>()
    lateinit var orderDetailsModel: OrderDetailsModel

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        products = findViewById(R.id.products)
        rate = findViewById(R.id.rate)
        Image = findViewById(R.id.Image)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        name = findViewById(R.id.name)
        address = findViewById(R.id.address)
        order_num = findViewById(R.id.order_num)
        location = findViewById(R.id.location)
        pay = findViewById(R.id.pay)

        service_type = findViewById(R.id.service_type)
        notes = findViewById(R.id.notes)
        follow = findViewById(R.id.follow)
        back.setOnClickListener { onBackPressed()
            finish()}
        products = findViewById(R.id.products)
        linearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL,false)
        countAdapter = CountAdapter(mContext,productsModels, R.layout.recycler_count)
        products.layoutManager=linearLayoutManager
        products.adapter = countAdapter
        getLocationWithPermission()
        getOrder()
        location.setOnClickListener {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("http://maps.google.com/maps?saddr=" + mLat + "," + mLang+"&daddr="+lat+","+lng)
                )
            )
        }


        follow.setOnClickListener {
            val intent = Intent(this,Invoice::class.java)
            intent.putExtra("id",id)
            intent.putExtra("category",orderDetailsModel.category)
            startActivity(intent)
        }
        rate.setOnClickListener {
            val intent = Intent(this,RateActivity::class.java)
            intent.putExtra("provider",orderDetailsModel.provider_id)
            intent.putExtra("delegate",orderDetailsModel.delegate_id)
            intent.putExtra("order",orderDetailsModel.id)
            startActivity(intent)
        }


    }
    fun setData(orderDetailsModel: OrderDetailsModel){
        Glide.with(mContext).load(orderDetailsModel.image).into(Image)
        name.text = orderDetailsModel.username
        lat = orderDetailsModel.lat.toString()
        lng = orderDetailsModel.lng.toString()
        order_num.text = orderDetailsModel.id.toString()
        for (i in 0..orderDetailsModel.items!!.size-1){
            if (orderDetailsModel.items!!.get(i).title.equals("address")){
                address.text = orderDetailsModel.items!!.get(i).details
                location.text = orderDetailsModel.items!!.get(i).details
            }else if (orderDetailsModel.items!!.get(i).title.equals("payment")){
                pay.text = orderDetailsModel.items!!.get(i).details
            }
            else if (orderDetailsModel.items!!.get(i).title.equals("notes")){
                notes.text = orderDetailsModel.items!!.get(i).details
            }
            else if (orderDetailsModel.items!!.get(i).title.equals("services")){
                service_type.text = orderDetailsModel.items!!.get(i).details
            }
        }


    }

    fun getOrder(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.OrderDetails(mLanguagePrefManager.appLanguage,id,mSharedPrefManager.userData.id!!,null
            ,"user")?.enqueue(object : Callback<OrderDetailsResponse> {
            override fun onFailure(call: Call<OrderDetailsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<OrderDetailsResponse>,
                response: Response<OrderDetailsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        setData(response.body()?.data!!)
                        orderDetailsModel = response.body()?.data!!
                        countAdapter.updateAll(response.body()?.data?.products!!)

                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
    fun getLocationWithPermission() {
        gps = GPSTracker(mContext, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext,
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                // putMapMarker(gps.getLatitude(), gps.getLongitude())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(this, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
//                        Toast.makeText(
//                            mContext,
//                            resources.getString(R.string.detect_location),
//                            Toast.LENGTH_SHORT
//                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }

                // googleMap.clear()
                // putMapMarker(gps.getLatitude(), gps.getLongitude())

            }
        }
    }
}