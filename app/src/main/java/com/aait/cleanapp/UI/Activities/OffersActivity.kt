package com.aait.cleanapp.UI.Activities

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.Client
import com.aait.cleanapp.Models.NotificationModel
import com.aait.cleanapp.Models.NotificationResponse
import com.aait.cleanapp.Models.OfferModel
import com.aait.cleanapp.Models.OffersResponse
import com.aait.cleanapp.Network.Service
import com.aait.cleanapp.R
import com.aait.cleanapp.UI.Adapters.NotificationAdapter
import com.aait.cleanapp.UI.Adapters.OffersAdapter
import com.aait.cleanapp.Uitls.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OffersActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_offers
    lateinit var back: ImageView


    lateinit var rv_recycle: RecyclerView
    var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    internal lateinit var linearLayoutManager: LinearLayoutManager
    internal var notificationModels = ArrayList<OfferModel>()
    internal lateinit var offersAdapter:OffersAdapter

    override fun initializeComponents() {
        back = findViewById(R.id.back)



        back.setOnClickListener { startActivity(Intent(this, MainActivity::class.java)) }
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        linearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)
        offersAdapter =
            OffersAdapter(mContext, notificationModels, R.layout.recycler_offers)

        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = offersAdapter
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener { getHome() }
        getHome()

    }

    fun getHome() {
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)
            ?.Offers(mLanguagePrefManager.appLanguage, mSharedPrefManager.userData.id!!)
            ?.enqueue(object :
                Callback<OffersResponse> {
                override fun onResponse(
                    call: Call<OffersResponse>,
                    response: Response<OffersResponse>
                ) {
                    swipeRefresh!!.isRefreshing = false
                    hideProgressDialog()
                    if (response.isSuccessful) {
                        try {
                            if (response.body()?.value.equals("1")) {
                                Log.e("myJobs", Gson().toJson(response.body()!!.data))
                                if (response.body()!!.data?.isEmpty()!!) {
                                    layNoItem!!.visibility = View.VISIBLE
                                    layNoInternet!!.visibility = View.GONE
                                    tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                                } else {
//                            initSliderAds(response.body()?.slider!!)
                                    offersAdapter.updateAll(response.body()!!.data!!)
                                }
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                            } else {
                                CommonUtil.makeToast(mContext, response.body()?.msg!!)
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    } else {
                    }
                }

                override fun onFailure(call: Call<OffersResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext, t)
                    t.printStackTrace()
                    layNoInternet!!.visibility = View.VISIBLE
                    layNoItem!!.visibility = View.GONE
                    swipeRefresh!!.isRefreshing = false
                    hideProgressDialog()
                    Log.e("response", Gson().toJson(t))
                }
            })


    }

}