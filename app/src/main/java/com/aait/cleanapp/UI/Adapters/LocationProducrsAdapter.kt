package com.aait.cleanapp.UI.Adapters

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.aait.cleanapp.Base.ParentRecyclerAdapter
import com.aait.cleanapp.Base.ParentRecyclerViewHolder
import com.aait.cleanapp.Models.Order
import com.aait.cleanapp.Models.ProductModel
import com.aait.cleanapp.R
import com.aait.cleanapp.UI.Activities.AirActivity
import com.aait.cleanapp.UI.Activities.LocationsServicesActivity
import com.bumptech.glide.Glide

class LocationProducrsAdapter (context: Context, data: MutableList<ProductModel>,id:String,name:String) :
    ParentRecyclerAdapter<ProductModel>(context, data,id,name) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(R.layout.recycler_location_products, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)
    var order = ArrayList<Order>()

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val bundleModel = data.get(position)
        viewHolder.name.text = bundleModel.name


        Glide.with(mcontext).asBitmap().load(bundleModel.image).into(viewHolder.image)
        viewHolder.itemView.setOnClickListener {
            if(name.equals(mcontext.getString(R.string.realestate))) {
                val intent = Intent(mcontext, LocationsServicesActivity::class.java)
                order.add(Order(bundleModel.subcategory_id, bundleModel.product_id.toString(), 1))
                intent.putExtra("product", bundleModel.product_id)
                intent.putExtra("id", id)
                intent.putExtra("order", order)
                mcontext.startActivity(intent)
            }else{
                val intent = Intent(mcontext, AirActivity::class.java)
                intent.putExtra("product", bundleModel.product_id)
                intent.putExtra("cat", bundleModel.subcategory_id)
                intent.putExtra("id",id)
                Log.e("id",id.toString())
                intent.putExtra("name", bundleModel.name)
                intent.putExtra("image", bundleModel.image)
                mcontext.startActivity(intent)

            }

        }



    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var name=itemView.findViewById<TextView>(R.id.name)

        internal var image = itemView.findViewById<ImageView>(R.id.image)


    }
}