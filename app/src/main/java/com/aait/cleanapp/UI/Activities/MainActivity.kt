package com.aait.cleanapp.UI.Activities


import android.Manifest
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.Client
import com.aait.cleanapp.GPS.GPSTracker
import com.aait.cleanapp.GPS.GpsTrakerListener
import com.aait.cleanapp.Listeners.OnItemClickListener
import com.aait.cleanapp.Models.*
import com.aait.cleanapp.Network.Service
import com.aait.cleanapp.R
import com.aait.cleanapp.UI.Adapters.CatsAdapter
import com.aait.cleanapp.Uitls.CommonUtil
import com.aait.cleanapp.Uitls.DialogUtil
import com.aait.cleanapp.Uitls.PermissionUtils
import com.bumptech.glide.Glide
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*

class MainActivity :Parent_Activity(), GoogleMap.OnMarkerClickListener, OnMapReadyCallback,
    GpsTrakerListener,OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {

            val intent = Intent(this@MainActivity, ProvidersActivity::class.java)
            intent.putExtra("lat" ,mLat)
            intent.putExtra("lng",mLang)
            intent.putExtra("category",categorie.get(position).id)
            intent.putExtra("cat",categorie.get(position).category_key)

            startActivity(intent)

    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        return false
    }

    override fun onMapReady(p0: GoogleMap?) {
        this.googleMap = p0!!
//        googleMap.setOnMapClickListener(this);
        googleMap.setOnMarkerClickListener(this)
        getLocationWithPermission()
    }

    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }

    override val layoutResource: Int
        get() = R.layout.activity_main
    lateinit var map: MapView
    lateinit var search:ImageView
    internal lateinit var googleMap: GoogleMap
    internal lateinit var myMarker: Marker
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    lateinit var count:TextView
    lateinit var count_not:TextView
    var mLang = ""
    var mLat = ""
    private var mAlertDialog: AlertDialog? = null
    lateinit var menu: ImageView
    lateinit var title: TextView
    lateinit var main: LinearLayout
    lateinit var profile: LinearLayout
    lateinit var orders: LinearLayout
    lateinit var Bundles: LinearLayout
    lateinit var wallet: LinearLayout
    lateinit var about_app: LinearLayout
    lateinit var terms: LinearLayout
    lateinit var complains: LinearLayout
    lateinit var settings: LinearLayout
    lateinit var share_app: LinearLayout
    lateinit var delegate:LinearLayout
    lateinit var provioder:LinearLayout
    lateinit var name:TextView
    lateinit var image: CircleImageView
    lateinit var logout: LinearLayout
    lateinit var search_text:EditText


    lateinit var cats_lay:LinearLayout
    lateinit var categories:RecyclerView
    lateinit var baskest:ImageView
    var categorie = ArrayList<ListModel>()
    lateinit var catsAdapter: CatsAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var notification:ImageView

    var deviceID = ""
    override fun initializeComponents() {
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        menu = findViewById(R.id.menu)
        title = findViewById(R.id.title)
        title.text = getString(R.string.main)
        baskest = findViewById(R.id.baskest)
        map = findViewById(R.id.map)
        search = findViewById(R.id.search)
        search_text = findViewById(R.id.search_text)
        count = findViewById(R.id.count)
        count_not = findViewById(R.id.count_not)
        search.setOnClickListener {
            if (search_text.text.toString().equals("")){
                CommonUtil.makeToast(mContext,getString(R.string.search_text))
            }else {
                val intent = Intent(this, SearchActivity::class.java)
                intent.putExtra("lat", mLat)
                intent.putExtra("lng", mLang)
                intent.putExtra("text", search_text.text.toString())
                startActivity(intent)
            }
        }

        notification = findViewById(R.id.notification)
        sideMenu()
        map.onCreate(mSavedInstanceState)
        map.onResume()
        map.getMapAsync(this)

        try {
            MapsInitializer.initialize(mContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        notification.setOnClickListener { startActivity(Intent(this,NotificationActivity::class.java)) }
        baskest.setOnClickListener { startActivity(Intent(this,CardActivity::class.java)) }
        getProfile()
        cats_lay = findViewById(R.id.cats_lay)
        categories = findViewById(R.id.categories)

        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        catsAdapter = CatsAdapter(mContext,categorie,R.layout.recycler_cats)
        catsAdapter.setOnItemClickListener(this)
        categories.layoutManager = linearLayoutManager
        categories.adapter = catsAdapter
        getCats()
        Log.e("user",Gson().toJson(mSharedPrefManager.userData))

    }
    fun AppCompatActivity.hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
        // else {
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        // }
    }

    override fun onResume() {
        super.onResume()
        Client.getClient()?.create(Service::class.java)?.Carts(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!)?.enqueue(object :
            Callback<CardResponse> {
            override fun onResponse(call: Call<CardResponse>, response: Response<CardResponse>) {

                hideProgressDialog()
                if (response.isSuccessful) {
                    try {
                        if (response.body()?.value.equals("1")) {
                            Log.e("myJobs", Gson().toJson(response.body()!!.data))
                            if (response.body()!!.data?.isEmpty()!!) {
                                count.visibility = View.GONE

                            } else {
//                            initSliderAds(response.body()?.slider!!)
                                count.visibility = View.VISIBLE
                                count.text = response.body()?.data?.size.toString()

                            }
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                        }else {}

                    } catch (e: Exception) {e.printStackTrace() }

                } else {  }
            }
            override fun onFailure(call: Call<CardResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()

                hideProgressDialog()
                Log.e("response", Gson().toJson(t))
            }
        })

        Client.getClient()?.create(Service::class.java)?.Count(mSharedPrefManager.userData.id!!)?.enqueue(object:Callback<CountResponse>{
            override fun onFailure(call: Call<CountResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<CountResponse>, response: Response<CountResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        count_not.text = response.body()?.data.toString()
                    }
                }
            }

        })
    }

    fun getCats(){
        showProgressDialog(getString(R.string.please_wait))
        hideKeyboard()
        Client.getClient()?.create(Service::class.java)?.categories(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!)?.enqueue(object :
            Callback<ListResponse> {
            override fun onFailure(call: Call<ListResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                Log.e("fail",Gson().toJson(t))
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<ListResponse>, response: Response<ListResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        catsAdapter.updateAll(response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
        Client.getClient()?.create(Service::class.java)?.Carts(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!)?.enqueue(object :
            Callback<CardResponse> {
            override fun onResponse(call: Call<CardResponse>, response: Response<CardResponse>) {

                hideProgressDialog()
                if (response.isSuccessful) {
                    try {
                        if (response.body()?.value.equals("1")) {
                            Log.e("myJobs", Gson().toJson(response.body()!!.data))
                            if (response.body()!!.data?.isEmpty()!!) {
                                count.visibility = View.GONE

                            } else {
//                            initSliderAds(response.body()?.slider!!)
                                count.visibility = View.VISIBLE
                                count.text = response.body()?.data?.size.toString()

                            }
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                        }else {}

                    } catch (e: Exception) {e.printStackTrace() }

                } else {  }
            }
            override fun onFailure(call: Call<CardResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()

                hideProgressDialog()
                Log.e("response", Gson().toJson(t))
            }
        })

        Client.getClient()?.create(Service::class.java)?.Count(mSharedPrefManager.userData.id!!)?.enqueue(object:Callback<CountResponse>{
            override fun onFailure(call: Call<CountResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<CountResponse>, response: Response<CountResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        count_not.text = response.body()?.data.toString()
                    }
                }
            }

        })
    }
    fun sideMenu(){
        drawer_layout.useCustomBehavior(Gravity.START)
        drawer_layout.useCustomBehavior(Gravity.END)
        drawer_layout.setRadius(Gravity.START, 80f)
        drawer_layout.setRadius(Gravity.END, 80f)
        drawer_layout.setViewScale(Gravity.START, 0.7f) //set height scale for main view (0f to 1f)
        drawer_layout.setViewScale(Gravity.END, 0.7f) //set height scale for main view (0f to 1f)
        drawer_layout.setViewElevation(Gravity.START, 0f) //set main view elevation when drawer open (dimension)
        drawer_layout.setViewElevation(Gravity.END, 0f) //set main view elevation when drawer open (dimension)
//        drawer_layout.setViewScrimColor(Gravity.START, Color.) //set drawer overlay coloe (color)
//        drawer_layout.setViewScrimColor(Gravity.END, Color.TRANSPARENT) //set drawer overlay coloe (color)
        drawer_layout.setDrawerElevation(Gravity.START, 80f) //set drawer elevation (dimension)
        drawer_layout.setDrawerElevation(Gravity.END, 80f) //set drawer elevation (dimension)
        drawer_layout.setContrastThreshold(3f) //set maximum of contrast ratio between white text and background color.
        drawer_layout.setRadius(Gravity.START, 0f) //set end container's corner radius (dimension)

        drawer_layout.setRadius(Gravity.END, 0f)

        menu.setOnClickListener { drawer_layout.openDrawer(GravityCompat.START) }

        name = drawer_layout.findViewById(R.id.name)
        image = drawer_layout.findViewById(R.id.image)
        main = drawer_layout.findViewById(R.id.main)
        profile = drawer_layout.findViewById(R.id.profile)
        orders = drawer_layout.findViewById(R.id.orders)
        delegate = drawer_layout.findViewById(R.id.delegate)
        Bundles = drawer_layout.findViewById(R.id.Bundles)
        wallet = drawer_layout.findViewById(R.id.wallet)
        provioder = drawer_layout.findViewById(R.id.provider)
        about_app = drawer_layout.findViewById(R.id.about_app)
        terms = drawer_layout.findViewById(R.id.terms)
        complains = drawer_layout.findViewById(R.id.complains)
        settings = drawer_layout.findViewById(R.id.settings)
        share_app = drawer_layout.findViewById(R.id.share_app)
        logout = drawer_layout.findViewById(R.id.logout)
        main.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
        this.finish()}
        name.text = mSharedPrefManager.userData.name
        Glide.with(mContext).asBitmap().load(mSharedPrefManager.userData.avatar).into(image)
        delegate.setOnClickListener {  startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://play.google.com/store/apps/details?id=com.aait.cleanappprovider")
            )
        ) }
        profile.setOnClickListener { startActivity(Intent(this@MainActivity,ProfileActivity::class.java)) }
        provioder.setOnClickListener {   startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://play.google.com/store/apps/details?id=com.aait.cleanappprovider")
            )
        )}
        settings.setOnClickListener { startActivity(Intent(this@MainActivity,SettingsActivity::class.java)) }
        about_app.setOnClickListener { startActivity(Intent(this@MainActivity,AboutAppActivity::class.java)) }
        terms.setOnClickListener { startActivity(Intent(this@MainActivity,TermsActivity::class.java)) }
        share_app.setOnClickListener { CommonUtil.ShareApp(mContext) }
        orders.setOnClickListener { startActivity(Intent(this,OrdersActivity::class.java)) }
        complains.setOnClickListener { startActivity(Intent(this@MainActivity,ComplainsActivity::class.java)) }
        Bundles.setOnClickListener { startActivity(Intent(this@MainActivity,BundleActivity::class.java)) }
        wallet.setOnClickListener { startActivity(Intent(this@MainActivity,WalletActivity::class.java)) }
        logout.setOnClickListener { logout() }
    }

    fun putMapMarker(lat: Double?, log: Double?) {
        val latLng = LatLng(lat!!, log!!)
        myMarker = googleMap.addMarker(
            MarkerOptions()
                .position(latLng)
                .title("موقعي")
                .icon(BitmapDescriptorFactory.fromResource(R.mipmap.map))
        )
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10f))
        // kkjgj

    }

    fun getLocationWithPermission() {
        gps = GPSTracker(mContext, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext,
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                putMapMarker(gps.getLatitude(), gps.getLongitude())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(this, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
                        Toast.makeText(
                            mContext,
                            resources.getString(R.string.detect_location),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        // mResult = addresses[0].getAddressLine(0)

                    }

                } catch (e: IOException) {
                }

                googleMap.clear()
                putMapMarker(gps.getLatitude(), gps.getLongitude())

            }
        }
    }
    fun logout(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.logOut(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,"android",mSharedPrefManager.userData.device_id!!)?.enqueue(object :Callback<AboutAppResponse>{
            override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {

                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<AboutAppResponse>,
                response: Response<AboutAppResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        mSharedPrefManager.loginStatus=false
                        mSharedPrefManager.Logout()
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        startActivity(Intent(this@MainActivity, SplashActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
    fun getProfile(){


        Client.getClient()?.create(Service::class.java)?.getProfile(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!)?.enqueue(object :
            Callback<UserResponse>{
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        mSharedPrefManager.userData = response.body()?.data!!

                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })

    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
    }
}
