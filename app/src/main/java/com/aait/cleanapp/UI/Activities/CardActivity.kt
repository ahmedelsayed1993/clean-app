package com.aait.cleanapp.UI.Activities

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.Client
import com.aait.cleanapp.Listeners.OnItemClickListener
import com.aait.cleanapp.Models.AboutAppResponse
import com.aait.cleanapp.Models.BaseResponse
import com.aait.cleanapp.Models.CardModel
import com.aait.cleanapp.Models.CardResponse
import com.aait.cleanapp.Network.Service
import com.aait.cleanapp.R
import com.aait.cleanapp.UI.Adapters.CardAdapter
import com.aait.cleanapp.Uitls.CommonUtil
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_complains.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CardActivity :Parent_Activity(),OnItemClickListener{
    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.delete){
            delete(cardModels.get(position).id!!)
        }else{
            if (cardModels.get(position)?.category_key.equals("clothes")){
                val intent = Intent(this,CardDetailsClothesActivity::class.java)
                intent.putExtra("id",cardModels.get(position)?.id)
                startActivity(intent)
            }else if (cardModels.get(position)?.category_key.equals("cars")){
                val intent = Intent(this,CardDetailsCarsActivity::class.java)
                intent.putExtra("id",cardModels.get(position)?.id)
                startActivity(intent)
            }else{
                val intent = Intent(this,CardDetailsLocationActivity::class.java)
                intent.putExtra("id",cardModels.get(position)?.id)
                startActivity(intent)
            }
        }

    }

    override val layoutResource: Int
        get() = R.layout.activity_card

    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null
    lateinit var num:TextView
    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    internal lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var cardAdapter: CardAdapter
    var cardModels = ArrayList<CardModel>()
    lateinit var back:ImageView
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        num = findViewById(R.id.num)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        cardAdapter = CardAdapter(mContext,cardModels)
        cardAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = cardAdapter
        back.setOnClickListener { onBackPressed()
            finish()}
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener { getHome() }
        getHome()

    }

    fun getHome(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Carts(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!)?.enqueue(object :
            Callback<CardResponse> {
            override fun onResponse(call: Call<CardResponse>, response: Response<CardResponse>) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {
                    try {
                        if (response.body()?.value.equals("1")) {
                            Log.e("myJobs", Gson().toJson(response.body()!!.data))
                            if (response.body()!!.data?.isEmpty()!!) {
                                num.visibility = View.GONE
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                                tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                            } else {
//                            initSliderAds(response.body()?.slider!!)
                                num.visibility = View.VISIBLE
                                num.text = response.body()?.data?.size.toString()+" "+getString(R.string.order)
                                cardAdapter.updateAll(response.body()!!.data!!)
                            }
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                        }else {}

                    } catch (e: Exception) {e.printStackTrace() }

                } else {  }
            }
            override fun onFailure(call: Call<CardResponse>, t: Throwable) {
                 CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                Log.e("response", Gson().toJson(t))
            }
        })
    }

    fun delete(id:Int){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.deleteCard(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id)
            ?.enqueue(object :Callback<AboutAppResponse>{
                override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<AboutAppResponse>,
                    response: Response<AboutAppResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            CommonUtil.makeToast(mContext,response.body()?.data!!)
                            getHome()
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }
}