package com.aait.cleanapp.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.aait.cleanapp.Base.ParentRecyclerAdapter
import com.aait.cleanapp.Base.ParentRecyclerViewHolder
import com.aait.cleanapp.Models.BundleModel
import com.aait.cleanapp.R

class BundleAdapter(context: Context, data: MutableList<BundleModel>, layoutId: Int) :
    ParentRecyclerAdapter<BundleModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val bundleModel = data.get(position)
        viewHolder.content!!.setText(bundleModel.content)
        viewHolder.price.text = bundleModel.price.toString()+"  "+mcontext.getString(R.string.sar )
        viewHolder.subscribe.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position) })



    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var price=itemView.findViewById<TextView>(R.id.price)
        internal var content = itemView.findViewById<TextView>(R.id.content)
        internal var subscribe = itemView.findViewById<Button>(R.id.subscribe)


    }
}