package com.aait.cleanapp.UI.Activities

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.Client
import com.aait.cleanapp.Listeners.OnItemClickListener
import com.aait.cleanapp.Models.SearchModel
import com.aait.cleanapp.Models.SearchResponse
import com.aait.cleanapp.Network.Service
import com.aait.cleanapp.R
import com.aait.cleanapp.UI.Adapters.SearchAdapter
import com.aait.cleanapp.Uitls.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SearchActivity :Parent_Activity(),OnItemClickListener{
    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(this,ProviderDetailsActivity::class.java)
        intent.putExtra("id",model.get(position).id)
        intent.putExtra("cat",model.get(position).category_key)
        startActivity(intent)
    }

    override val layoutResource: Int
        get() = R.layout.activity_search
    lateinit var back:ImageView
    lateinit var text:EditText
    lateinit var search:ImageView
    lateinit var rv_recycle: RecyclerView
    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    var swipeRefresh: SwipeRefreshLayout? = null
    internal lateinit var linearLayoutManager: LinearLayoutManager
    var model = ArrayList<SearchModel>()
    lateinit var searchAdapter: SearchAdapter
    var lat = ""
    var lng = ""
    var t = ""

    override fun initializeComponents() {
        lat = intent.getStringExtra("lat")
        lng = intent.getStringExtra("lng")
        t = intent.getStringExtra("text")
        back = findViewById(R.id.back)
        text = findViewById(R.id.text)
        search = findViewById(R.id.search)
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoInternet = findViewById(R.id.lay_no_internet)
        layNoItem = findViewById(R.id.lay_no_item)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        back.setOnClickListener { onBackPressed() }
        text.setText(t)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        searchAdapter = SearchAdapter(mContext,model,R.layout.recycler_providers)
        searchAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = searchAdapter
        search.setOnClickListener {
            if (text.text.toString().equals("")){
                CommonUtil.makeToast(mContext,getString(R.string.search_text))
            }else{
                search(text.text.toString())
            }
        }
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener { text.setText("")}
        if (t.toString().equals("")){

        }else {
            search(t)
        }


    }
    fun search(text:String){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.Search(mLanguagePrefManager.appLanguage,text,lat,lng)?.enqueue(object :
            Callback<SearchResponse>{
            override fun onFailure(call: Call<SearchResponse>, t: Throwable) {
                 CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()
                layNoInternet!!.visibility = View.VISIBLE
                layNoItem!!.visibility = View.GONE
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                Log.e("response", Gson().toJson(t))
            }

            override fun onResponse(
                call: Call<SearchResponse>,
                response: Response<SearchResponse>
            ) {
                swipeRefresh!!.isRefreshing = false
                hideProgressDialog()
                if (response.isSuccessful) {
                    try {
                        if (response.body()?.value.equals("1")) {
                            Log.e("myJobs", Gson().toJson(response.body()!!.data))
                            if (response.body()!!.data?.isEmpty()!!) {
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                                tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                            } else {
//                            initSliderAds(response.body()?.slider!!)
                                searchAdapter.updateAll(response.body()!!.data!!)
                            }
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                        }else {}

                    } catch (e: Exception) {e.printStackTrace() }

                } else {  }
            }
        })
    }
}