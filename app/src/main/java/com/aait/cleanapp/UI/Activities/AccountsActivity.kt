package com.aait.cleanapp.UI.Activities

import android.content.Intent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.Client
import com.aait.cleanapp.Models.AccountsModel
import com.aait.cleanapp.Models.AccountsResponse
import com.aait.cleanapp.Network.Service
import com.aait.cleanapp.R
import com.aait.cleanapp.UI.Adapters.AccountsAdapter
import com.aait.cleanapp.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AccountsActivity :Parent_Activity(){
    override val layoutResource: Int
        get() = R.layout.activity_accounts
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var menu:ImageView
    lateinit var accounts:RecyclerView
    lateinit var accountsAdapter: AccountsAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var confirm:Button
    var accountsModels = ArrayList<AccountsModel>()
    var price = 0

    override fun initializeComponents() {
        price = intent.getIntExtra("price",0)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        menu = findViewById(R.id.menu)
        accounts = findViewById(R.id.accounts)
        accountsAdapter = AccountsAdapter(mContext,accountsModels,R.layout.recycler_accounts)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        confirm = findViewById(R.id.confirm)
        menu.visibility = View.GONE
        title.text = getString(R.string.bank_accounts)
        back.setOnClickListener { onBackPressed()
            finish()}
        accounts.layoutManager = linearLayoutManager
        accounts.adapter = accountsAdapter
        getHome()
        confirm.setOnClickListener {
            val intent = Intent(this,BankTransferActivity::class.java)
            intent.putExtra("price",price)
            startActivity(intent)
        }

    }
    fun getHome(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getAccounts(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!)?.enqueue(object :
            Callback<AccountsResponse>{
            override fun onFailure(call: Call<AccountsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<AccountsResponse>,
                response: Response<AccountsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        accountsAdapter.updateAll(response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
}