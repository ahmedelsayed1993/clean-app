package com.aait.cleanapp.UI.Activities

import android.content.Intent
import android.widget.ImageView
import android.widget.TextView
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.R

class ThreeActivity :Parent_Activity(){
    override val layoutResource: Int
        get() = R.layout.activity_three
    lateinit var back: ImageView
    lateinit var skip: TextView
    lateinit var next: ImageView
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        skip = findViewById(R.id.skip)
        next = findViewById(R.id.next)
        back.setOnClickListener { onBackPressed() }
        skip.setOnClickListener { startActivity(Intent(this@ThreeActivity,LoginActivity::class.java)) }
        next.setOnClickListener { startActivity(Intent(this@ThreeActivity,FourActivity::class.java))  }
    }
}