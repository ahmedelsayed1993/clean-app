package com.aait.cleanapp.UI.Activities

import android.content.Intent
import android.widget.ImageView
import android.widget.TextView
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.Client
import com.aait.cleanapp.Models.AboutAppResponse
import com.aait.cleanapp.Network.Service
import com.aait.cleanapp.R
import com.aait.cleanapp.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WalletActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_wallet
    lateinit var menu:ImageView
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var balance:TextView

    override fun initializeComponents() {
        menu = findViewById(R.id.menu)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        balance = findViewById(R.id.balance)
        title.text = getString(R.string.Portfolio)
        menu.setOnClickListener { onBackPressed()
        finish()}
        back.setOnClickListener { startActivity(Intent(this@WalletActivity,MainActivity::class.java))
        finish()}
        wallet()


    }
    fun wallet(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.balance(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!)?.enqueue(object :
            Callback<AboutAppResponse> {
            override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<AboutAppResponse>,
                response: Response<AboutAppResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        balance.text = response.body()?.data!!+" "+getString(R.string.rs)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}