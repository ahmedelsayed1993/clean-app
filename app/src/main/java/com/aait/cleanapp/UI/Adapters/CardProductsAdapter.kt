package com.aait.cleanapp.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.aait.cleanapp.Base.ParentRecyclerAdapter
import com.aait.cleanapp.Base.ParentRecyclerViewHolder
import com.aait.cleanapp.Models.CardProductModel
import com.aait.cleanapp.Models.ProductModel
import com.aait.cleanapp.R
import com.bumptech.glide.Glide

class CardProductsAdapter (context: Context, data: MutableList<CardProductModel>) :
    ParentRecyclerAdapter<CardProductModel>(context, data) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(R.layout.recycler_clothes_products, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val bundleModel = data.get(position)
        viewHolder.name.text = bundleModel.name

        viewHolder.count.text = bundleModel.count.toString()
        Glide.with(mcontext).asBitmap().load(bundleModel.image).into(viewHolder.image)
//        viewHolder.add.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)
//            bundleModel.count = bundleModel.count?.plus(1)
//        viewHolder.count.text = bundleModel.count.toString()})
//        viewHolder.minus.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position)
//        if (bundleModel.count==0){
//
//        }else{
//            bundleModel.count = bundleModel.count?.minus(1)
//            viewHolder.count.text = bundleModel.count.toString()
//        }})
        viewHolder.add.setOnClickListener {
            bundleModel.count = bundleModel.count?.toInt()?.plus(1).toString()
            viewHolder.count.text = bundleModel.count.toString()
        }
        viewHolder.minus.setOnClickListener {
            if (bundleModel.count!!.equals("1")){

            }else{
                bundleModel.count = bundleModel.count?.toInt()?.minus(1).toString()
                viewHolder.count.text = bundleModel.count.toString()
            }
        }


    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var name=itemView.findViewById<TextView>(R.id.name)
        internal var count = itemView.findViewById<TextView>(R.id.count)
        internal var minus = itemView.findViewById<ImageView>(R.id.minus)
        internal var add = itemView.findViewById<ImageView>(R.id.add)
        internal var image = itemView.findViewById<ImageView>(R.id.image)


    }
}