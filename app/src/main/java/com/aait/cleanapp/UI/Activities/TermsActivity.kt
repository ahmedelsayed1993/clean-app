package com.aait.cleanapp.UI.Activities

import android.content.Intent
import android.widget.ImageView
import android.widget.TextView
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.Client
import com.aait.cleanapp.Models.AboutAppResponse
import com.aait.cleanapp.Network.Service
import com.aait.cleanapp.R
import com.aait.cleanapp.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TermsActivity :Parent_Activity(){
    override val layoutResource: Int
        get() = R.layout.activity_about_app
    lateinit var title:TextView
    lateinit var about:TextView
    lateinit var menu: ImageView
    lateinit var back: ImageView
    override fun initializeComponents() {
        menu = findViewById(R.id.menu)
        back = findViewById(R.id.back)
        menu.setOnClickListener { onBackPressed()
            this.finish()}
        back.setOnClickListener { startActivity(Intent(this, MainActivity::class.java))
            this.finish()}
        title = findViewById(R.id.title)
        title.text = getString(R.string.terms_conditions)
        about = findViewById(R.id.about)
        About()

    }

    fun About(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Terms(mLanguagePrefManager.appLanguage)?.enqueue(object :
            Callback<AboutAppResponse> {
            override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<AboutAppResponse>,
                response: Response<AboutAppResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        about.text = response.body()?.data
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
}