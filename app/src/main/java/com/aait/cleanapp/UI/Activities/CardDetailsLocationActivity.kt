package com.aait.cleanapp.UI.Activities

import android.content.Intent
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.Client
import com.aait.cleanapp.Models.CardDetailsOrderResponse
import com.aait.cleanapp.Models.Order
import com.aait.cleanapp.Network.Service
import com.aait.cleanapp.R
import com.aait.cleanapp.Uitls.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CardDetailsLocationActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_card_details_location
    lateinit var back: ImageView
    lateinit var location: TextView
    lateinit var car_size: TextView

    lateinit var clean_type: TextView
    lateinit var complete: Button
    var sizes = ArrayList<String>()
    var service = ArrayList<Meter>()
    var count = ArrayList<String>()
    var id = 0
    var num = 0
    var order = ArrayList<Order>()
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        location = findViewById(R.id.location)
        car_size = findViewById(R.id.car_size)
        back.setOnClickListener { onBackPressed()
        finish()}
        clean_type = findViewById(R.id.clean_type)
        complete = findViewById(R.id.complete)
        getData()
        complete.setOnClickListener {
            val intent = Intent(this, UpdateOrderLocation::class.java)
            intent.putExtra("services", service)
            intent.putExtra("products", order)
            intent.putExtra("id", id)
            intent.putExtra("category","locations")
            startActivity(intent)
        }

    }
    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getCardDetails(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id)
            ?.enqueue(object : Callback<CardDetailsOrderResponse> {
                override fun onFailure(call: Call<CardDetailsOrderResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<CardDetailsOrderResponse>,
                    response: Response<CardDetailsOrderResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            if (response.body()?.details_order?.address.equals("")){
                                location.text = getString(R.string.not_detected)
                            }else {
                                location.text = response.body()?.details_order?.address
                            }

                            for (i in 0..response.body()?.data?.get(0)?.products!!.size-1){
                                order.add(Order(response.body()?.data?.get(0)?.subcategory,response.body()?.data?.get(0)?.products?.get(i)?.id.toString(),response.body()?.data?.get(0)?.products?.get(i)?.count!!.toInt()))
                                sizes.add(response.body()?.data?.get(0)?.products!!.get(i)?.name!!)

                            }
                            for (i in 0..response.body()?.services!!.size-1){
                                if (response.body()?.services?.get(i)?.checked!=0) {
                                    count.add(response.body()?.services?.get(i)?.name!!)
                                    service.add(Meter(response.body()?.services?.get(i)?.id!!.toString(),response.body()?.services?.get(i)?.meter!!.toInt()))
                                }
                            }
                            Log.e("services",Gson().toJson(service))
                            clean_type.text = count.joinToString().replace(", ","_")
                            car_size.text = sizes.joinToString().replace(", ","_")

                        }
                    }
                }

            })
    }

}