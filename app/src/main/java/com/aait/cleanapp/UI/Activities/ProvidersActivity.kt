package com.aait.cleanapp.UI.Activities

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.Client
import com.aait.cleanapp.Listeners.OnItemClickListener
import com.aait.cleanapp.Models.*
import com.aait.cleanapp.Network.Service
import com.aait.cleanapp.R
import com.aait.cleanapp.UI.Adapters.ProvidersAdapter
import com.aait.cleanapp.UI.Adapters.SliderAdapter
import com.aait.cleanapp.Uitls.CommonUtil
import com.github.islamkhsh.CardSliderViewPager
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.security.Provider

class ProvidersActivity :Parent_Activity(),OnItemClickListener{
    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(this,ProviderDetailsActivity::class.java)
        intent.putExtra("id",providerModels.get(position).id)
        intent.putExtra("cat",cat)
        startActivity(intent)
    }

    fun AppCompatActivity.hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
        // else {
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        // }
    }

    override val layoutResource: Int
        get() = R.layout.activity_providers
    lateinit var providers:RecyclerView
    lateinit var viewPager:CardSliderViewPager
    lateinit var providersAdapter: ProvidersAdapter
    var providerModels = ArrayList<ProviderModel>()
    lateinit var linearLayoutManager: LinearLayoutManager
    var lat = ""
    var lng = ""
    var category_id = 0
    var cat = ""
    lateinit var search_text:EditText
    lateinit var search:ImageView
    lateinit var filter:ImageView
    lateinit var menu:ImageView
    lateinit var basket:ImageView
    lateinit var count: TextView
    lateinit var count_not: TextView
    lateinit var notification:ImageView

    override fun initializeComponents() {
        lat = intent.getStringExtra("lat")
        lng = intent.getStringExtra("lng")
        category_id = intent.getIntExtra("category",0)
        cat = intent.getStringExtra("cat")
        menu = findViewById(R.id.menu)
        menu.setImageResource(R.mipmap.bac_1)
        count = findViewById(R.id.count)
        count_not = findViewById(R.id.count_not)
        providers = findViewById(R.id.providers)
        viewPager = findViewById(R.id.viewPager)
        search_text = findViewById(R.id.search_text)
        search = findViewById(R.id.search)
        filter = findViewById(R.id.filter)
        if (mLanguagePrefManager.appLanguage.equals("ar")){
            menu.rotation = 180F
        }else{
            menu.rotation = 0F
        }
        menu.setOnClickListener { onBackPressed()
        finish()}
        basket = findViewById(R.id.baskest)
        notification = findViewById(R.id.notification)
        basket.setOnClickListener { startActivity(Intent(this,CardActivity::class.java))
        finish()}
        notification.setOnClickListener {
            startActivity(Intent(this,NotificationActivity::class.java))
            finish()
        }
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        providersAdapter = ProvidersAdapter(mContext,providerModels,R.layout.recycler_providers)
        providersAdapter.setOnItemClickListener(this)
        providers.layoutManager = linearLayoutManager
        providers.adapter = providersAdapter
        Providers()
        search.setOnClickListener {
            if (search_text.text.toString().equals("")){
                CommonUtil.makeToast(mContext,getString(R.string.search_text))
            }else {
                val intent = Intent(this, SearchActivity::class.java)
                intent.putExtra("lat", lat)
                intent.putExtra("lng", lng)
                intent.putExtra("text", search_text.text.toString())
                startActivity(intent)
            }
        }
        filter.setOnClickListener {
            val popupMenu = PopupMenu(mContext, filter)

            popupMenu.inflate(R.menu.menu)
            popupMenu.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.top -> {
                        filter(1)
                        return@OnMenuItemClickListener true
                    }
                    R.id.low -> {
                        filter(null)
                        return@OnMenuItemClickListener true
                    }
                }
                false
            })

            popupMenu.show()
        }


    }
    fun initSliderAds(list:ArrayList<BannersModel>){
        if(list.isEmpty()){
            viewPager.visibility= View.GONE

        }
        else{
            viewPager.visibility= View.VISIBLE

            viewPager.adapter= SliderAdapter(mContext,list)

        }
    }
    fun filter(rate:Int?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Filter(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!
        ,category_id,rate,lat,lng)?.enqueue(object :
            Callback<ProviderResponse>{
            override fun onFailure(call: Call<ProviderResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<ProviderResponse>,
                response: Response<ProviderResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        initSliderAds(response.body()?.banners!!)
                        providersAdapter.updateAll(response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    override fun onResume() {
        super.onResume()
        Client.getClient()?.create(Service::class.java)?.Carts(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!)?.enqueue(object :
            Callback<CardResponse> {
            override fun onResponse(call: Call<CardResponse>, response: Response<CardResponse>) {

                hideProgressDialog()
                if (response.isSuccessful) {
                    try {
                        if (response.body()?.value.equals("1")) {
                            Log.e("myJobs", Gson().toJson(response.body()!!.data))
                            if (response.body()!!.data?.isEmpty()!!) {
                                count.visibility = View.GONE

                            } else {
//                            initSliderAds(response.body()?.slider!!)
                                count.visibility = View.VISIBLE
                                count.text = response.body()?.data?.size.toString()

                            }
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                        }else {}

                    } catch (e: Exception) {e.printStackTrace() }

                } else {  }
            }
            override fun onFailure(call: Call<CardResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()

                hideProgressDialog()
                Log.e("response", Gson().toJson(t))
            }
        })

        Client.getClient()?.create(Service::class.java)?.Count(mSharedPrefManager.userData.id!!)?.enqueue(object:Callback<CountResponse>{
            override fun onFailure(call: Call<CountResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<CountResponse>, response: Response<CountResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        count_not.text = response.body()?.data.toString()
                    }
                }
            }

        })

    }

    fun Providers(){
        showProgressDialog(getString(R.string.please_wait))
        hideKeyboard()
        Client.getClient()?.create(Service::class.java)?.getProviders(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,category_id,lat,lng)?.enqueue(object :
            Callback<ProviderResponse>{
            override fun onFailure(call: Call<ProviderResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<ProviderResponse>,
                response: Response<ProviderResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        initSliderAds(response.body()?.banners!!)
                        providersAdapter.updateAll(response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
        Client.getClient()?.create(Service::class.java)?.Carts(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!)?.enqueue(object :
            Callback<CardResponse> {
            override fun onResponse(call: Call<CardResponse>, response: Response<CardResponse>) {

                hideProgressDialog()
                if (response.isSuccessful) {
                    try {
                        if (response.body()?.value.equals("1")) {
                            Log.e("myJobs", Gson().toJson(response.body()!!.data))
                            if (response.body()!!.data?.isEmpty()!!) {
                                count.visibility = View.GONE

                            } else {
//                            initSliderAds(response.body()?.slider!!)
                                count.visibility = View.VISIBLE
                                count.text = response.body()?.data?.size.toString()

                            }
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                        }else {}

                    } catch (e: Exception) {e.printStackTrace() }

                } else {  }
            }
            override fun onFailure(call: Call<CardResponse>, t: Throwable) {
                CommonUtil.handleException(mContext!!,t)
                t.printStackTrace()

                hideProgressDialog()
                Log.e("response", Gson().toJson(t))
            }
        })

        Client.getClient()?.create(Service::class.java)?.Count(mSharedPrefManager.userData.id!!)?.enqueue(object:Callback<CountResponse>{
            override fun onFailure(call: Call<CountResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<CountResponse>, response: Response<CountResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        count_not.text = response.body()?.data.toString()
                    }
                }
            }

        })
    }
}