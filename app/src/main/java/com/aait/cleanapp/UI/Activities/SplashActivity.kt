package com.aait.cleanapp.UI.Activities

import android.content.Intent
import android.os.Handler
import android.view.animation.AnimationUtils
import android.widget.ImageView
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.R

class SplashActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_splash
    var isSplashFinishid = false

    lateinit var logo : ImageView
    override fun initializeComponents() {
        logo = findViewById(R.id.logo)


        val logoAnimation2 = AnimationUtils.loadAnimation(this, R.anim.anim_shine)

        Handler().postDelayed({
            logo.startAnimation(logoAnimation2)
            Handler().postDelayed({
                isSplashFinishid=true
                if (mSharedPrefManager.loginStatus==true){

                    startActivity(Intent(this@SplashActivity,MainActivity::class.java))
                    this@SplashActivity.finish()

                }else {

                    var intent = Intent(this@SplashActivity, OneActivity::class.java)
                    startActivity(intent)
                    finish()
                }

            }, 2100)
        }, 1800)
    }


}