package com.aait.cleanapp.UI.Activities

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.Client
import com.aait.cleanapp.Listeners.OnItemClickListener
import com.aait.cleanapp.Models.*
import com.aait.cleanapp.Network.Service
import com.aait.cleanapp.R
import com.aait.cleanapp.UI.Adapters.ClothesAdapter
import com.aait.cleanapp.UI.Adapters.ServicesAdapter
import com.aait.cleanapp.Uitls.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ClothesSubCategoryActivity:Parent_Activity(),OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.service){
            if (servicesModels.get(position).checked==0){
                servicesModels.get(position).checked = 1
                ServicesModel1.add(ListModel(servicesModels.get(position)?.id,servicesModels.get(position)?.name))
                servicesAdapter.notifyDataSetChanged()
                Log.e("list", Gson().toJson(ServicesModel1))
            }else{
                servicesModels.get(position).checked = 0
                Log.e("position",position.toString())
                if (ServicesModel1.size>1) {
                    for (i in 0..ServicesModel1.size - 1){
                        Log.e("rrr",ServicesModel1.get(i).id.toString())
                        Log.e("ttt",servicesModels.get(position)?.id.toString())
                        if (ServicesModel1.get(i).id==servicesModels.get(position)?.id){
                            var list = ListModel(ServicesModel1.get(i).id,ServicesModel1.get(i).name)
                            ServicesModel1.removeAt(
                                i
                            )
                            break
                        }
                    }

                }else if (ServicesModel1.size==1){
                    ServicesModel1.clear()
                }

                servicesAdapter.notifyDataSetChanged()
                Log.e("list1", Gson().toJson(ServicesModel1))
            }
        }


    }

    override val layoutResource: Int
        get() = R.layout.activity_clothes_subcats
    lateinit var cats:RecyclerView
    lateinit var clothesAdapter: ClothesAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    var subCategoriesModels = ArrayList<SubCategoriesModel>()
    lateinit var linearLayoutManager1: LinearLayoutManager
    var servicesModels = ArrayList<ServicesModel>()
    var ServicesModel1 = ArrayList<ListModel>()
    lateinit var servicesAdapter: ServicesAdapter
    lateinit var services:RecyclerView
    var id = 0
    var category = ""
    lateinit var text:TextView
    lateinit var text1:TextView
    lateinit var request:Button
    lateinit var basket:Button
    var products = ArrayList<Order>()
    var Services = ArrayList<Int>()
    lateinit var back:ImageView

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        category = intent.getStringExtra("category")
        cats = findViewById(R.id.cats)
        back = findViewById(R.id.back)
        back.setOnClickListener { onBackPressed()
            finish()}
        services = findViewById(R.id.services)
        request = findViewById(R.id.request)
        basket = findViewById(R.id.basket)
        text = findViewById(R.id.text)
        text1 = findViewById(R.id.text1)
        text.visibility = View.GONE
        text1.visibility = View.GONE
        clothesAdapter = ClothesAdapter(mContext,subCategoriesModels)
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        linearLayoutManager1 = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        servicesAdapter = ServicesAdapter(mContext,servicesModels,R.layout.recycler_services)

        servicesAdapter.setOnItemClickListener(this)
        cats.layoutManager = linearLayoutManager
        cats.adapter = clothesAdapter
        services.layoutManager = linearLayoutManager1
        services.adapter = servicesAdapter
        getClothes()
        request.setOnClickListener {
            for (i in 0..clothesAdapter.getData()?.size - 1) {
                for (j in 0..clothesAdapter.getData()?.get(i)?.products?.size!! - 1) {
                    if (clothesAdapter.getData()?.get(i)?.products?.get(j)?.count!! != 0) {
                        products.add(
                            Order(
                                clothesAdapter.getData()?.get(i).subcategory_id,
                                clothesAdapter.getData()?.get(i)?.products?.get(j)?.product_id.toString()
                                ,
                                clothesAdapter.getData()?.get(i)?.products?.get(j)?.count
                            )
                        )

                    }

                }
            }
            Log.e("products", Gson().toJson(products))
            if (!ServicesModel1.isEmpty()) {
                for (i in 0..ServicesModel1?.size - 1) {

                    Services.add(ServicesModel1?.get(i)?.id!!)
                }
                Log.e("Services", Gson().toJson(Services))
            }

            if (products.isEmpty()) {
                CommonUtil.makeToast(mContext, getString(R.string.choose_clothes))
            } else {
                if (Services.isEmpty()) {
                    CommonUtil.makeToast(mContext, getString(R.string.choose_service))
                } else {
                    val intent = Intent(this, RequestOrderActivity::class.java)
                    intent.putExtra("services", Services.joinToString().replace(" ", ""))
                    intent.putExtra("products", products)
                    intent.putExtra("id", id)
                    intent.putExtra("category",category)
                    startActivity(intent)
                }
            }
        }

        basket.setOnClickListener {
            for (i in 0..clothesAdapter.getData()?.size - 1) {
                for (j in 0..clothesAdapter.getData()?.get(i)?.products?.size!! - 1) {
                    if (clothesAdapter.getData()?.get(i)?.products?.get(j)?.count!! != 0) {
                        products.add(
                            Order(
                                clothesAdapter.getData()?.get(i).subcategory_id,
                                clothesAdapter.getData()?.get(i)?.products?.get(j)?.product_id.toString()
                                ,
                                clothesAdapter.getData()?.get(i)?.products?.get(j)?.count
                            )
                        )

                    }

                }
            }
            Log.e("products", Gson().toJson(products))
            if (!ServicesModel1.isEmpty()) {
                for (i in 0..ServicesModel1?.size - 1) {

                    Services.add(ServicesModel1?.get(i)?.id!!)
                }
                Log.e("Services", Gson().toJson(Services))
            }

            if (products.isEmpty()) {
                CommonUtil.makeToast(mContext, getString(R.string.choose_clothes))
            } else {
                if (Services.isEmpty()) {
                    CommonUtil.makeToast(mContext, getString(R.string.choose_service))
                } else {
                    AddToBasket(Gson().toJson(products),Services.joinToString().replace(" ",""))
                }
            }
        }

    }

    fun getClothes(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.SubCats(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id)?.enqueue(object :
            Callback<SubCategoriesResponse>{
            override fun onFailure(call: Call<SubCategoriesResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<SubCategoriesResponse>,
                response: Response<SubCategoriesResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if(response.body()?.data!!.isEmpty()||response.body()?.services!!.isEmpty()){
                            request.visibility = View.GONE
                            basket.visibility = View.GONE
                            text.visibility = View.VISIBLE
                            text1.visibility = View.VISIBLE
                        }else if (response.body()?.services!!.isEmpty()) {
                            request.visibility = View.GONE
                            basket.visibility = View.GONE
                            text1.visibility = View.VISIBLE
                        }else{
                            text.visibility =View.GONE
                            text1.visibility = View.GONE
                            request.visibility = View.VISIBLE
                            basket.visibility = View.VISIBLE
                            clothesAdapter.updateAll(response.body()?.data!!)
                            servicesAdapter.updateAll(response.body()?.services!!)
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }

    fun AddToBasket(order:String,services:String){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.AddToCard(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id,order,services,0)
            ?.enqueue(object :Callback<AddOrderResponse>{
                override fun onFailure(call: Call<AddOrderResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<AddOrderResponse>,
                    response: Response<AddOrderResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            CommonUtil.makeToast(mContext,getString(R.string.order_added))
                            onBackPressed()
                            finish()
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }
}