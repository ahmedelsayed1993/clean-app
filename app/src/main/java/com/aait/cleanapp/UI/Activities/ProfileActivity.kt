package com.aait.cleanapp.UI.Activities

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.text.InputType
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.Client
import com.aait.cleanapp.Models.BaseResponse
import com.aait.cleanapp.Models.UserResponse
import com.aait.cleanapp.Network.Service
import com.aait.cleanapp.R
import com.aait.cleanapp.Uitls.CommonUtil
import com.aait.cleanapp.Uitls.PermissionUtils
import com.bumptech.glide.Glide
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import de.hdodenhof.circleimageview.CircleImageView
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class ProfileActivity : Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_profile
    lateinit var menu:ImageView
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var image:CircleImageView
    lateinit var change:ImageView
    lateinit var name:TextView
    lateinit var Name:EditText
    lateinit var location:TextView
    lateinit var phone:EditText
    lateinit var password:TextView
    lateinit var membership:ImageView
    lateinit var confirm:Button
    lateinit var text:TextView
    lateinit var membership_name:TextView
     var lat:String? = null
    var lng:String? = null
    var address = ""
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    private var ImageBasePath: String? = null

    override fun initializeComponents() {
        menu = findViewById(R.id.menu)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        image = findViewById(R.id.image)
        change = findViewById(R.id.change)
        name = findViewById(R.id.name)
        Name = findViewById(R.id.Name)
        location = findViewById(R.id.location)
        phone = findViewById(R.id.phone)
        password = findViewById(R.id.password)
        membership = findViewById(R.id.membership)
        confirm = findViewById(R.id.confirm)
        text = findViewById(R.id.text)
        membership_name = findViewById(R.id.membership_name)
        menu.setOnClickListener { onBackPressed()
        finish()}
        back.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
        finish()}
        title.text = getString(R.string.profile)
        getProfile()
        image.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }
        change.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }

        location.setOnClickListener {
            startActivityForResult(Intent(this@ProfileActivity,LocationActivity::class.java),1)
        }

        confirm.setOnClickListener {
            if (CommonUtil.checkEditError(Name,getString(R.string.enter_name))||
                CommonUtil.checkTextError(location,getString(R.string.Please_specify_the_address))||
                CommonUtil.checkEditError(phone,getString(R.string.enter_phone))||
                CommonUtil.checkLength(phone,getString(R.string.phone_length),9)){
                return@setOnClickListener
            }else{
                updateProfile()
            }
        }

        password.setOnClickListener {
            val dialog = Dialog(mContext)
            dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog ?.setCancelable(true)
            dialog ?.setContentView(R.layout.dialog_change_password)
            val old_pass = dialog?.findViewById<EditText>(R.id.old_pass)
            val new_pass = dialog?.findViewById<EditText>(R.id.new_pass)
            val confirm_pass = dialog?.findViewById<EditText>(R.id.confirm_pass)
            val view = dialog?.findViewById<ImageView>(R.id.view)
            val view_pass = dialog?.findViewById<ImageView>(R.id.view_pass)
            val view_confirm = dialog?.findViewById<ImageView>(R.id.view_confirm)
            val save = dialog?.findViewById<Button>(R.id.save)
            view?.setOnClickListener { if (old_pass?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL){
                old_pass?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

            }else{
                old_pass?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL
            }
            }
            view_pass?.setOnClickListener { if (new_pass?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL){
                new_pass?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

            }else{
                new_pass?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL
            }
            }
            view_confirm?.setOnClickListener { if (confirm_pass?.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL){
                confirm_pass?.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

            }else{
                confirm_pass?.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL
            }
            }

            save?.setOnClickListener {
                if (CommonUtil.checkEditError(old_pass,getString(R.string.old_password))||
                        CommonUtil.checkEditError(new_pass,getString(R.string.new_password))||
                        CommonUtil.checkLength(new_pass,getString(R.string.password_length),6)||
                        CommonUtil.checkEditError(confirm_pass,getString(R.string.confirm_password))){
                    return@setOnClickListener
                }else{
                    if (!new_pass.text.toString().equals(confirm_pass.text.toString())){
                       confirm_pass.error = getString(R.string.password_not_match)
                    }else{
                        showProgressDialog(getString(R.string.please_wait))
                        Client.getClient()?.create(Service::class.java)?.resetPassword(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,old_pass.text.toString(),new_pass.text.toString())?.enqueue(
                            object :Callback<BaseResponse>{
                                override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                                    CommonUtil.handleException(mContext,t)
                                    t.printStackTrace()
                                    hideProgressDialog()
                                }

                                override fun onResponse(
                                    call: Call<BaseResponse>,
                                    response: Response<BaseResponse>
                                ) {
                                    hideProgressDialog()
                                    if (response.isSuccessful){
                                        if (response.body()?.value.equals("1")){
                                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                            dialog?.dismiss()
                                        }else{
                                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                                            dialog?.dismiss()
                                        }
                                    }
                                }
                            }
                        )
                    }
                }

            }
            dialog?.show()
        }
        membership.setOnClickListener {

            startActivity(Intent(this,OffersActivity::class.java))
            finish()
        }

    }

    fun getProfile(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getProfile(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!)?.enqueue(object :
            Callback<UserResponse>{
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        mSharedPrefManager.userData = response.body()?.data!!
                        Glide.with(mContext).load(response.body()?.data?.avatar).into(image)
                        name.text = response.body()?.data?.name
                        Name.setText(response.body()?.data?.name)
                        location.text = response.body()?.data?.national_address
                        address = response.body()?.data?.national_address!!
                        lat = response.body()?.data?.lat
                        lng = response.body()?.data?.lng
                        phone.setText(response.body()?.data?.phone)
                        if (response.body()?.data?.star_image.equals("")){
                            membership.visibility = View.GONE
                            text.visibility = View.GONE
                            membership_name.visibility = View.GONE
                        }else{
                            text.visibility = View.VISIBLE
                            membership.visibility = View.VISIBLE
                            membership_name.visibility = View.VISIBLE
                            membership_name.text = response.body()?.data?.star_name
                            Glide.with(mContext).load(response.body()?.data?.star_image).into(membership)
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

            ImageBasePath = returnValue!![0]

            // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);
            image!!.setImageURI(Uri.parse(ImageBasePath))
            if (ImageBasePath != null) {

               addAvater(ImageBasePath!!)
            }
        }else{
            address = data?.getStringExtra("result").toString()
            lat = data?.getStringExtra("lat").toString()
            lng = data?.getStringExtra("lng").toString()
            location.text = address
        }


    }

    fun addAvater(path:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.name, fileBody)
        Client.getClient()?.create(Service::class.java)?.updateAvatar(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,filePart)?.enqueue(object :
            Callback<UserResponse> {
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                 CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()


            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        mSharedPrefManager.userData = response.body()?.data!!
                        Glide.with(mContext).load(response.body()?.data?.avatar).into(image)
                        name.text = response.body()?.data?.name
                        Name.setText(response.body()?.data?.name)
                        location.text = response.body()?.data?.national_address
                        address = response.body()?.data?.national_address!!
                        lat = response.body()?.data?.lat!!
                        lng = response.body()?.data?.lng!!
                        phone.setText(response.body()?.data?.phone)
                        if (response.body()?.data?.star_image.equals("")){
                            membership.visibility = View.GONE
                            text.visibility = View.GONE
                            membership_name.visibility = View.GONE
                        }else{
                            text.visibility = View.VISIBLE
                            membership.visibility = View.VISIBLE
                            membership_name.visibility = View.VISIBLE
                            membership_name.text = response.body()?.data?.star_name
                            Glide.with(mContext).load(response.body()?.data?.star_image).into(membership)
                        }

                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }

    fun updateProfile(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.updateprofile(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,lat!!,lng!!,address,Name.text.toString(),phone.text.toString())
            ?.enqueue(object :Callback<UserResponse>{
                override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<UserResponse>,
                    response: Response<UserResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            mSharedPrefManager.userData = response.body()?.data!!
                            Glide.with(mContext).load(response.body()?.data?.avatar).into(image)
                            name.text = response.body()?.data?.name
                            Name.setText(response.body()?.data?.name)
                            location.text = response.body()?.data?.national_address
                            address = response.body()?.data?.national_address!!
                            lat = response.body()?.data?.lat
                            lng = response.body()?.data?.lng
                            phone.setText(response.body()?.data?.phone)
                            if (response.body()?.data?.star_image.equals("")){
                                membership.visibility = View.GONE
                                text.visibility = View.GONE
                                membership_name.visibility = View.GONE
                            }else{
                                text.visibility = View.VISIBLE
                                membership.visibility = View.VISIBLE
                                membership_name.visibility = View.VISIBLE
                                membership_name.text = response.body()?.data?.star_name
                                Glide.with(mContext).load(response.body()?.data?.star_image).into(membership)
                            }
                            startActivity(Intent(this@ProfileActivity,MainActivity::class.java))
                            finish()
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }
}