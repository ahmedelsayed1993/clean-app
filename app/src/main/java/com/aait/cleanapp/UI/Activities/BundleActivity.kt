package com.aait.cleanapp.UI.Activities

import android.content.Intent
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.Client
import com.aait.cleanapp.Listeners.OnItemClickListener

import com.aait.cleanapp.Models.BundleModel
import com.aait.cleanapp.Models.BundleResponse
import com.aait.cleanapp.Network.Service
import com.aait.cleanapp.R
import com.aait.cleanapp.UI.Adapters.BundleAdapter
import com.aait.cleanapp.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BundleActivity : Parent_Activity(),OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(this,AccountsActivity::class.java)
        intent.putExtra("price",bundleModels.get(position).price)
        startActivity(intent)

    }

    override val layoutResource: Int
        get() = R.layout.activity_bundle
    lateinit var rv_recycle: RecyclerView

    internal var layNoInternet: RelativeLayout? = null

    internal var layNoItem: RelativeLayout? = null

    internal var tvNoContent: TextView? = null

    internal var swipeRefresh: SwipeRefreshLayout? = null
    internal var bundleModels= ArrayList<BundleModel>()
    internal lateinit var bundleAdapter:BundleAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var title:TextView
    lateinit var menu: ImageView
    lateinit var back: ImageView
    override fun initializeComponents() {
        title = findViewById(R.id.title)
        menu = findViewById(R.id.menu)
        back = findViewById(R.id.back)
        rv_recycle = findViewById(R.id.rv_recycle)
        layNoItem = findViewById(R.id.lay_no_item)
        layNoInternet = findViewById(R.id.lay_no_internet)
        tvNoContent = findViewById(R.id.tv_no_content)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        back.setOnClickListener { startActivity(Intent(this@BundleActivity,MainActivity::class.java))
        finish()}
        menu.setOnClickListener { onBackPressed()
        finish()}
        title.text = getString(R.string.Bundles)
        linearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL,false)
        bundleAdapter = BundleAdapter(mContext,bundleModels,R.layout.recycler_bundle)
        bundleAdapter.setOnItemClickListener(this)
        rv_recycle.layoutManager = linearLayoutManager
        rv_recycle.adapter = bundleAdapter
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener { getPackages() }
        getPackages()

    }

    fun getPackages(){
        showProgressDialog(getString(R.string.please_wait))
        layNoInternet!!.visibility = View.GONE
        layNoItem!!.visibility = View.GONE
        Client.getClient()?.create(Service::class.java)?.pakages(mLanguagePrefManager.appLanguage)?.enqueue(
            object : Callback<BundleResponse> {
                override fun onFailure(call: Call<BundleResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    layNoInternet!!.visibility = View.VISIBLE
                    layNoItem!!.visibility = View.GONE
                    swipeRefresh!!.isRefreshing = false
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<BundleResponse>,
                    response: Response<BundleResponse>
                ) {
                    hideProgressDialog()
                    swipeRefresh!!.isRefreshing = false
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            if (response.body()?.data?.isEmpty()!!){
                                layNoItem!!.visibility = View.VISIBLE
                                layNoInternet!!.visibility = View.GONE
                                tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                            }else{

                                bundleAdapter.updateAll(response.body()?.data!!)
                            }
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            }
        )
    }
}