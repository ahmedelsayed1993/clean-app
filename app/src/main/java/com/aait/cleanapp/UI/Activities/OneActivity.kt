package com.aait.cleanapp.UI.Activities

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.provider.Settings
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.GPS.GPSTracker
import com.aait.cleanapp.GPS.GpsTrakerListener
import com.aait.cleanapp.R
import com.aait.cleanapp.Uitls.CommonUtil
import com.aait.cleanapp.Uitls.DialogUtil
import com.aait.cleanapp.Uitls.PermissionUtils
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import java.io.IOException
import java.util.*

class OneActivity:Parent_Activity(),GpsTrakerListener {
    override fun onTrackerSuccess(lat: Double?, log: Double?) {
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog()
                // putMapMarker(lat, log)
            }
        }
    }

    override fun onStartTracker() {
        startTracker = true
    }
    override val layoutResource: Int
        get() = R.layout.activity_one
    lateinit var back:ImageView
    lateinit var skip:TextView
    lateinit var next:ImageView
    lateinit var english:Button
    lateinit var arabic:Button
    internal lateinit var googleMap: GoogleMap
    internal lateinit var myMarker: Marker
    internal lateinit var geocoder: Geocoder
    internal lateinit var gps: GPSTracker
    internal var startTracker = false
    var mLang = ""
    var mLat = ""
    var result = ""


    private var mAlertDialog: AlertDialog? = null

    override fun initializeComponents() {
        back = findViewById(R.id.back)
        skip = findViewById(R.id.skip)
        next = findViewById(R.id.next)
        english = findViewById(R.id.english)
        arabic = findViewById(R.id.arabic)
        back.setOnClickListener { onBackPressed() }
        skip.setOnClickListener {  startActivity(Intent(this@OneActivity,LoginActivity::class.java))}
        next.setOnClickListener { startActivity(Intent(this@OneActivity,TwoActivity::class.java)) }
        getLocationWithPermission()
        arabic.setOnClickListener { mLanguagePrefManager.appLanguage = "ar"
            finishAffinity()
            startActivity(Intent(this@OneActivity, SplashActivity::class.java))
        }
        english.setOnClickListener {
            mLanguagePrefManager.appLanguage = "en"
            finishAffinity()
            startActivity(Intent(this@OneActivity,SplashActivity::class.java))
        }
    }
    fun getLocationWithPermission() {
        gps = GPSTracker(mContext, this)
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.ACCESS_FINE_LOCATION)&&
                        (PermissionUtils.hasPermissions(mContext,
                            Manifest.permission.ACCESS_COARSE_LOCATION)))) {
                CommonUtil.PrintLogE("Permission not granted")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(
                        PermissionUtils.GPS_PERMISSION,
                        800
                    )
                    Log.e("GPS", "1")
                }
            } else {
                getCurrentLocation()
                Log.e("GPS", "2")
            }
        } else {
            Log.e("GPS", "3")
            getCurrentLocation()
        }

    }

    internal fun getCurrentLocation() {
        gps.getLocation()
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(mContext,
                getString(R.string.gps_detecting),
                DialogInterface.OnClickListener { dialogInterface, i ->
                    mAlertDialog?.dismiss()
                    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                    startActivityForResult(intent, 300)
                })
        } else {
            if (gps.getLatitude() !== 0.0 && gps.getLongitude() !== 0.0) {
                // putMapMarker(gps.getLatitude(), gps.getLongitude())
                mLat = gps.getLatitude().toString()
                mLang = gps.getLongitude().toString()
                val addresses: List<Address>
                geocoder = Geocoder(this, Locale.getDefault())
                try {
                    addresses = geocoder.getFromLocation(
                        java.lang.Double.parseDouble(mLat),
                        java.lang.Double.parseDouble(mLang),
                        1
                    )
                    if (addresses.isEmpty()) {
//                        Toast.makeText(
//                            mContext,
//                            resources.getString(R.string.detect_location),
//                            Toast.LENGTH_SHORT
//                        ).show()
                    } else {
                        result = addresses[0].getAddressLine(0)
                        Log.e("address",result)
                        // CommonUtil.makeToast(mContext,addresses[0].getAddressLine(0))



                    }

                } catch (e: IOException) {
                }

                // googleMap.clear()
                // putMapMarker(gps.getLatitude(), gps.getLongitude())

            }
        }
    }
}