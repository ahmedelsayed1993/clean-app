package com.aait.cleanapp.UI.Activities

import android.content.Intent
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.Client
import com.aait.cleanapp.Models.CodeResponse
import com.aait.cleanapp.Models.InvoiceResponse
import com.aait.cleanapp.Models.ItemInvoiceModel
import com.aait.cleanapp.Network.Service
import com.aait.cleanapp.R
import com.aait.cleanapp.UI.Adapters.InvoiceAdapter
import com.aait.cleanapp.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class InvoiceActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_invoice

    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var items:RecyclerView
    lateinit var vat:TextView
    lateinit var vat_price:TextView
    lateinit var delivery_price:TextView
    lateinit var total:TextView
    var itemInvoiceModels = ArrayList<ItemInvoiceModel>()
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var invoiceAdapter: InvoiceAdapter
    lateinit var payment:Button
    lateinit var delivery:LinearLayout
    lateinit var discount:EditText
    lateinit var msg:TextView
    lateinit var final_total:TextView
    lateinit var value:TextView
     var swipeRefresh: SwipeRefreshLayout? = null
    var id = 0
    var category = ""
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        category = intent.getStringExtra("category")
        value = findViewById(R.id.value)
        swipeRefresh = findViewById(R.id.swipe_refresh)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        items = findViewById(R.id.items)
        vat = findViewById(R.id.vat)
        payment = findViewById(R.id.payment)
        vat_price = findViewById(R.id.vat_price)
        delivery = findViewById(R.id.delivery)
        delivery_price = findViewById(R.id.delivery_price)
        total = findViewById(R.id.total)
        discount = findViewById(R.id.discount)
        final_total = findViewById(R.id.final_total)
        msg = findViewById(R.id.msg)
        title.text = getString(R.string.invoice)
        back.setOnClickListener { onBackPressed()
        finish()}
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        invoiceAdapter = InvoiceAdapter(mContext,itemInvoiceModels,R.layout.recycler_invoice)
        items.layoutManager = linearLayoutManager
        items.adapter = invoiceAdapter
        back.setOnClickListener { onBackPressed()
        finish()}
        if (category.equals("clothes")){
            delivery.visibility = View.VISIBLE
        }else{
            delivery.visibility = View.GONE
        }
        swipeRefresh!!.setColorSchemeResources(
            R.color.colorPrimary,
            R.color.colorPrimaryDark,
            R.color.colorAccent
        )
        swipeRefresh!!.setOnRefreshListener { Invoice()
            msg.visibility = View.GONE
            final_total.visibility = View.GONE
            discount.setText("")
            value.visibility = View.GONE
            CommonUtil.removeStrokInText(total)
        }
        Invoice()

        payment.setOnClickListener {
            if(discount.text.toString().equals("")){
                val intent = Intent(this@InvoiceActivity,PaymentActivity::class.java)
                intent.putExtra("id",id)
                intent.putExtra("category",category)
                startActivity(intent)
            }else{
                code(1)
            }
        }
        discount.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView, actionId: Int, event: KeyEvent?): Boolean {
                if (event != null && event!!.getKeyCode() === KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
                    if (discount.text.toString().equals("")){
                        Invoice()
                        msg.visibility = View.GONE
                        final_total.visibility = View.GONE
                        discount.setText("")
                        value.visibility = View.GONE
                        CommonUtil.removeStrokInText(total)
                    }else {
                        code()
                    }
                }
                return false
            }
        })

    }

    fun Invoice(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getInvoice(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id)?.enqueue(
            object : Callback<InvoiceResponse>{

                override fun onFailure(call: Call<InvoiceResponse>, t: Throwable) {
                    swipeRefresh!!.isRefreshing = false
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<InvoiceResponse>,
                    response: Response<InvoiceResponse>
                ) {
                    swipeRefresh!!.isRefreshing = false
                   hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            vat.text = response.body()?.tax+" % "
                            vat_price.text = response.body()?.total_tax +" "+ getString(R.string.rs)
                            total.text = response.body()?.total +" "+getString(R.string.rs)
                            delivery_price.text = response.body()?.delivery +" "+getString(R.string.rs)
                            invoiceAdapter.updateAll(response.body()?.data!!)
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            }
        )
    }
    fun code(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Discount(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id,discount.text.toString(),null)?.enqueue(object :Callback<CodeResponse>{
            override fun onFailure(call: Call<CodeResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<CodeResponse>, response: Response<CodeResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        total.text = response.body()?.total_order.toString()+""+getString(R.string.rs)
                        CommonUtil.setStrokInText(total)
                        final_total.visibility = View.VISIBLE
                        final_total.text = response.body()?.data.toString()+" "+getString(R.string.rs)
                        value.visibility = View.VISIBLE
                        value.text = getString(R.string.discount_value)+":"+response.body()?.discount+"%"

                        msg.visibility = View.GONE
                    }else{
                        msg.visibility = View.VISIBLE
                        final_total.visibility = View.GONE
                        msg.text = response.body()?.msg!!
                        value.visibility = View.GONE
                    }
                }
            }

        })
    }
    fun code(confirm:Int?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Discount(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id,discount.text.toString(),confirm)?.enqueue(object :Callback<CodeResponse>{
            override fun onFailure(call: Call<CodeResponse>, t: Throwable) {
                hideProgressDialog()
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
            }

            override fun onResponse(call: Call<CodeResponse>, response: Response<CodeResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        val intent = Intent(this@InvoiceActivity,PaymentActivity::class.java)
                        intent.putExtra("id",id)
                        intent.putExtra("category",category)
                        startActivity(intent)
                        total.text = response.body()?.total_order.toString()+""+getString(R.string.rs)
                        CommonUtil.setStrokInText(total)
                        final_total.visibility = View.VISIBLE
                        final_total.text = response.body()?.data.toString()+" "+getString(R.string.rs)
                        value.visibility = View.VISIBLE
                        value.text = getString(R.string.discount_value)+":"+response.body()?.discount+"%"

                        msg.visibility = View.GONE
                    }else{
                        msg.visibility = View.VISIBLE
                        final_total.visibility = View.GONE
                        msg.text = response.body()?.msg!!
                        value.visibility = View.GONE
                    }
                }
            }

        })
    }
}