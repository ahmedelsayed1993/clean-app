package com.aait.cleanapp.UI.Activities

import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.Client
import com.aait.cleanapp.Models.BannersModel
import com.aait.cleanapp.Models.ProviderDetailsModel
import com.aait.cleanapp.Models.ProviderDetailsResponse
import com.aait.cleanapp.Network.Service
import com.aait.cleanapp.R
import com.aait.cleanapp.UI.Adapters.SliderAdapter
import com.aait.cleanapp.Uitls.CommonUtil
import com.github.islamkhsh.CardSliderIndicator
import com.github.islamkhsh.CardSliderViewPager
import me.relex.circleindicator.CircleIndicator
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProviderDetailsActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_provider_details
    lateinit var back:ImageView
    lateinit var viewPager: CardSliderViewPager
    lateinit var indicator: CircleIndicator
    lateinit var name:TextView
    lateinit var share:ImageView
    lateinit var address:TextView
    lateinit var rating:RatingBar
    lateinit var description:TextView
    lateinit var des:TextView
    lateinit var reserve:Button
    lateinit var price:TextView
    var cat = ""
    var id = 0
    var link = ""
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        cat = intent.getStringExtra("cat")
        back = findViewById(R.id.back)
        viewPager = findViewById(R.id.viewPager)
        indicator = findViewById(R.id.indicator)
        name = findViewById(R.id.name)
        share = findViewById(R.id.share)
        address = findViewById(R.id.address)
        rating = findViewById(R.id.rating)
        price = findViewById(R.id.price)
        description = findViewById(R.id.description)
        reserve = findViewById(R.id.reserve)
        des = findViewById(R.id.des)
        back.setOnClickListener { onBackPressed()
        finish()}
        getProvider()
        share.setOnClickListener {
            if (!link.equals(""))
            {
                if (link.startsWith("http"))
                {
                    Log.e("here", "333")
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(link)
                    startActivity(i)

                } else {
                    Log.e("here", "4444")
                    val url = "https://$link"
                    val i = Intent(Intent.ACTION_VIEW)
                    i.data = Uri.parse(url)
                    startActivity(i)
                }
            }
        }
        reserve.setOnClickListener {
            if (cat.equals("clothes")) {
                val intent = Intent(this, ClothesSubCategoryActivity::class.java)
                intent.putExtra("category","clothes")
                intent.putExtra("id", id)
                startActivity(intent)
            }else if (cat.equals("cars")){
                val intent = Intent(this, CarsActivity::class.java)
                intent.putExtra("category","cars")
                intent.putExtra("id", id)
                startActivity(intent)
            }else{
                val intent = Intent(this, LoctionsActivity::class.java)
                intent.putExtra("id", id)
                startActivity(intent)
            }
        }

    }
    fun initSliderAds(list:ArrayList<BannersModel>){
        if(list.isEmpty()){
            viewPager.visibility= View.GONE
            indicator.visibility = View.GONE

        }
        else{
            viewPager.visibility= View.VISIBLE
            indicator.visibility=View.VISIBLE
            viewPager.adapter= SliderAdapter(mContext,list)
            indicator.setViewPager(viewPager)
        }
    }
    fun setData(providerDetailsModel: ProviderDetailsModel){
        name.text = providerDetailsModel.name
        address.text = providerDetailsModel.address
        if (providerDetailsModel.description.equals("")){
            description.visibility = View.GONE
            des.visibility = View.GONE
        }else {
            description.visibility = View.VISIBLE
            des.visibility = View.VISIBLE
            description.text = providerDetailsModel.description
        }
        rating.rating = providerDetailsModel.rate!!
        link = providerDetailsModel.share_link!!

    }
    fun getProvider(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getProvider(mLanguagePrefManager.appLanguage,id)?.enqueue(object :
            Callback<ProviderDetailsResponse>{
            override fun onFailure(call: Call<ProviderDetailsResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<ProviderDetailsResponse>,
                response: Response<ProviderDetailsResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        initSliderAds(response.body()?.data?.images!!)
                        setData(response.body()?.data!!)
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}