package com.aait.cleanapp.UI.Adapters

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.cleanapp.Base.ParentRecyclerAdapter
import com.aait.cleanapp.Base.ParentRecyclerViewHolder
import com.aait.cleanapp.Models.ServicesModel
import com.aait.cleanapp.R

class LocationServicesAdapter (context: Context, data: MutableList<ServicesModel>, layoutId: Int) :
    ParentRecyclerAdapter<ServicesModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)
var selected = 0
    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val listModel = data.get(position)
        viewHolder.name!!.setText(listModel.name)
        viewHolder.name.isChecked = (selected==position)
        viewHolder.name.tag = selected
        viewHolder.count.setText(listModel.count)
        viewHolder.count.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {


            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                listModel?.count = p0.toString()
            }

        })
        viewHolder.name.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position) })



    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {

        internal var name=itemView.findViewById<CheckBox>(R.id.name)
        internal var count = itemView.findViewById<EditText>(R.id.count)


    }
}