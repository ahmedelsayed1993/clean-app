package com.aait.cleanapp.UI.Activities

import android.content.Intent
import android.widget.Button
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.R

class BackHomeActivity :Parent_Activity(){
    override val layoutResource: Int
        get() = R.layout.activity_back_to_main
    lateinit var follow:Button
    lateinit var back:Button
    var id = 0

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        follow = findViewById(R.id.follow)
        back = findViewById(R.id.back)
        back.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
        finish()}
        follow.setOnClickListener { startActivity(Intent(this,OrdersActivity::class.java))
        finish()}

    }
}