package com.aait.cleanapp.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.aait.cleanapp.Base.ParentRecyclerAdapter
import com.aait.cleanapp.Base.ParentRecyclerViewHolder
import com.aait.cleanapp.Models.ListModel
import com.aait.cleanapp.Models.ProductsModel
import com.aait.cleanapp.R

class CountAdapter (context: Context, data: MutableList<ProductsModel>, layoutId: Int) :
    ParentRecyclerAdapter<ProductsModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val listModel = data.get(position)
        viewHolder.cat!!.setText(listModel.product)
        viewHolder.count.text = listModel.count




    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var cat=itemView.findViewById<TextView>(R.id.cat)
        internal var count = itemView.findViewById<TextView>(R.id.count)


    }
}