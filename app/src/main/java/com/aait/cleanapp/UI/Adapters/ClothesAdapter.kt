package com.aait.cleanapp.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.cleanapp.Base.ParentRecyclerAdapter
import com.aait.cleanapp.Base.ParentRecyclerViewHolder
import com.aait.cleanapp.Listeners.OnItemClickListener
import com.aait.cleanapp.Models.ProductModel
import com.aait.cleanapp.Models.SubCategoriesModel
import com.aait.cleanapp.R
import com.bumptech.glide.Glide

class ClothesAdapter (context: Context, data: MutableList<SubCategoriesModel>) :
    ParentRecyclerAdapter<SubCategoriesModel>(context, data) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(R.layout.recycler_clothes, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val bundleModel = data.get(position)
        viewHolder.name.text = bundleModel.subcategory_name
        viewHolder.productsAdapter = ProductsAdapter(mcontext,viewHolder.productModels)
        viewHolder.products.layoutManager = LinearLayoutManager(mcontext,LinearLayoutManager.HORIZONTAL,false)
        viewHolder.products.adapter = viewHolder.productsAdapter
        viewHolder.productsAdapter.updateAll(bundleModel.products!!)
        Glide.with(mcontext).asBitmap().load(bundleModel.subcategory_image).into(viewHolder.image)




    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {




         lateinit var productsAdapter:ProductsAdapter
        internal var productModels = ArrayList<ProductModel>()
        internal var name=itemView.findViewById<TextView>(R.id.name)
        internal var products = itemView.findViewById<RecyclerView>(R.id.products)
        internal var image = itemView.findViewById<ImageView>(R.id.image)


    }
}