package com.aait.cleanapp.UI.Activities

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.Client
import com.aait.cleanapp.Listeners.OnItemClickListener
import com.aait.cleanapp.Models.AboutAppResponse
import com.aait.cleanapp.Models.NotificationModel
import com.aait.cleanapp.Models.NotificationResponse
import com.aait.cleanapp.Network.Service
import com.aait.cleanapp.R
import com.aait.cleanapp.UI.Adapters.NotificationAdapter
import com.aait.cleanapp.Uitls.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NotificationActivity:Parent_Activity(),OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.delete) {
            showProgressDialog(getString(R.string.please_wait))
            Client.getClient()?.create(Service::class.java)?.delete(
                mLanguagePrefManager.appLanguage, mSharedPrefManager.userData.id!!
                , notificationModels.get(position).id!!
            )?.enqueue(object :
                Callback<AboutAppResponse> {
                override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext, t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<AboutAppResponse>,
                    response: Response<AboutAppResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful) {
                        if (response.body()?.value.equals("1")) {
                            CommonUtil.makeToast(mContext, response.body()?.data!!)
                            getHome()
                        } else {
                            CommonUtil.makeToast(mContext, response.body()?.msg!!)
                        }
                    }
                }

            })
        }else if (view.id == R.id.lay){
            if (notificationModels.get(position)?.type.equals("rate")){

                val intent = Intent(this,RateActivity::class.java)
                intent.putExtra("provider",notificationModels.get(position).provider_id)
                intent.putExtra("delegate",notificationModels.get(position).delegate_id)
                intent.putExtra("order",notificationModels.get(position).order_id)
                startActivity(intent)
            }else{
                val intent = Intent(this, OrderDetailsActivity::class.java)
                intent.putExtra("id",notificationModels.get(position).order_id)
                intent.putExtra("status",notificationModels.get(position).status_order)
                startActivity(intent)
            }
        }
    }

    override val layoutResource: Int
        get() = R.layout.activity_notifications


        lateinit var back: ImageView

        lateinit var title: TextView
        lateinit var rv_recycle: RecyclerView
        var layNoInternet: RelativeLayout? = null

        internal var layNoItem: RelativeLayout? = null

        internal var tvNoContent: TextView? = null

        var swipeRefresh: SwipeRefreshLayout? = null
        internal lateinit var linearLayoutManager: LinearLayoutManager
        internal var notificationModels = java.util.ArrayList<NotificationModel>()
        internal lateinit var notificationAdapter: NotificationAdapter

        override fun initializeComponents() {
            back = findViewById(R.id.back)

            title = findViewById(R.id.title)
            title.text = getString(R.string.notification)
            back.setOnClickListener { startActivity(Intent(this, MainActivity::class.java)) }
            rv_recycle = findViewById(R.id.rv_recycle)
            layNoInternet = findViewById(R.id.lay_no_internet)
            layNoItem = findViewById(R.id.lay_no_item)
            tvNoContent = findViewById(R.id.tv_no_content)
            swipeRefresh = findViewById(R.id.swipe_refresh)
            linearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)
            notificationAdapter =
                NotificationAdapter(mContext, notificationModels, R.layout.recycler_notifications)
            notificationAdapter.setOnItemClickListener(this)
            rv_recycle.layoutManager = linearLayoutManager
            rv_recycle.adapter = notificationAdapter
            swipeRefresh!!.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimaryDark,
                R.color.colorAccent
            )
            swipeRefresh!!.setOnRefreshListener { getHome() }
            getHome()

        }

        fun getHome() {
            showProgressDialog(getString(R.string.please_wait))
            layNoInternet!!.visibility = View.GONE
            layNoItem!!.visibility = View.GONE
            Client.getClient()?.create(Service::class.java)
                ?.Notification(mLanguagePrefManager.appLanguage, mSharedPrefManager.userData.id!!)
                ?.enqueue(object :
                    Callback<NotificationResponse> {
                    override fun onResponse(
                        call: Call<NotificationResponse>,
                        response: Response<NotificationResponse>
                    ) {
                        swipeRefresh!!.isRefreshing = false
                        hideProgressDialog()
                        if (response.isSuccessful) {
                            try {
                                if (response.body()?.value.equals("1")) {
                                    Log.e("myJobs", Gson().toJson(response.body()!!.data))
                                    if (response.body()!!.data?.isEmpty()!!) {
                                        layNoItem!!.visibility = View.VISIBLE
                                        layNoInternet!!.visibility = View.GONE
                                        tvNoContent!!.setText(R.string.content_not_found_you_can_still_search_the_app_freely)
                                    } else {
//                            initSliderAds(response.body()?.slider!!)
                                        notificationAdapter.updateAll(response.body()!!.data!!)
                                    }
//                            response.body()!!.data?.let { homeSeekerAdapter.updateAll(it)
                                } else {
                                    CommonUtil.makeToast(mContext, response.body()?.msg!!)
                                }

                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                        } else {
                        }
                    }

                    override fun onFailure(call: Call<NotificationResponse>, t: Throwable) {
                        CommonUtil.handleException(mContext, t)
                        t.printStackTrace()
                        layNoInternet!!.visibility = View.VISIBLE
                        layNoItem!!.visibility = View.GONE
                        swipeRefresh!!.isRefreshing = false
                        hideProgressDialog()
                        Log.e("response", Gson().toJson(t))
                    }
                })


        }

        override fun onBackPressed() {
            super.onBackPressed()
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
}

