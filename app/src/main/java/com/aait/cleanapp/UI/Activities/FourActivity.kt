package com.aait.cleanapp.UI.Activities

import android.content.Intent
import android.widget.ImageView
import android.widget.TextView
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.R

class FourActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_four

    lateinit var back: ImageView
    lateinit var skip: TextView
    lateinit var next: ImageView
    override fun initializeComponents() {
        back = findViewById(R.id.back)
        skip = findViewById(R.id.skip)
        next = findViewById(R.id.next)
        back.setOnClickListener { onBackPressed() }
        skip.setOnClickListener { startActivity(Intent(this@FourActivity,LoginActivity::class.java)) }
        next.setOnClickListener { startActivity(Intent(this@FourActivity,LoginActivity::class.java)) }
    }
}