package com.aait.cleanapp.UI.Activities

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.Client
import com.aait.cleanapp.Listeners.OnItemClickListener
import com.aait.cleanapp.Models.*
import com.aait.cleanapp.Network.Service
import com.aait.cleanapp.R
import com.aait.cleanapp.UI.Adapters.CarServicesAdapter
import com.aait.cleanapp.Uitls.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CarServicesActivity :Parent_Activity(),OnItemClickListener{
    override fun onItemClick(view: View, position: Int) {
        if (servicesModels.get(position).checked==0){
            servicesModels.get(position).checked = 1
            carServicesAdapter.selected = position
            ServicesModel1.add(ListModel(servicesModels.get(position)?.id,servicesModels.get(position)?.name))
            carServicesAdapter.notifyDataSetChanged()
            service = servicesModels.get(position)?.id!!
            Log.e("list", Gson().toJson(ServicesModel1))
        }else{
            servicesModels.get(position).checked = 0

            Log.e("position",position.toString())
            if (ServicesModel1.size>1) {
                for (i in 0..ServicesModel1.size - 1){
                    Log.e("rrr",ServicesModel1.get(i).id.toString())
                    Log.e("ttt",servicesModels.get(position)?.id.toString())
                    if (ServicesModel1.get(i).id==servicesModels.get(position)?.id){
                        var list = ListModel(ServicesModel1.get(i).id,ServicesModel1.get(i).name)
                        ServicesModel1.removeAt(
                            i
                        )
                        break
                    }
                }

            }else if (ServicesModel1.size==1){
                ServicesModel1.clear()
            }

            carServicesAdapter.notifyDataSetChanged()
            Log.e("list1", Gson().toJson(ServicesModel1))
        }
    }

    override val layoutResource: Int
        get() = R.layout.activity_car_services
    lateinit var cats:RecyclerView
    lateinit var back:ImageView
    lateinit var title:TextView
    lateinit var request:Button
    lateinit var basket:Button
    lateinit var carServicesAdapter: CarServicesAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    var servicesModels = ArrayList<ServicesModel>()
    var servicesModels1 = ArrayList<ServicesModel>()
    var products = ArrayList<Order>()
    var ServicesModel1 = ArrayList<ListModel>()
    var Services = ArrayList<Int>()
    var id = 0
    var service = 0
    var category = ""

    override fun initializeComponents() {
        servicesModels1 = intent.getSerializableExtra("services") as ArrayList<ServicesModel>
        products = intent.getSerializableExtra("products") as ArrayList<Order>
        id = intent.getIntExtra("id",0)
        category = intent.getStringExtra("category")
        cats = findViewById(R.id.cats)
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        request = findViewById(R.id.request)
        basket = findViewById(R.id.basket)
        title.text = getString(R.string.cats)
        service = servicesModels1.get(0).id!!
        back.setOnClickListener { onBackPressed()
            finish()}
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        carServicesAdapter = CarServicesAdapter(mContext,servicesModels,R.layout.recycler_car_services)
        carServicesAdapter.setOnItemClickListener(this)
        cats.layoutManager = linearLayoutManager
        cats.adapter = carServicesAdapter
        carServicesAdapter.updateAll(servicesModels1)

        request.setOnClickListener {
            if (!ServicesModel1.isEmpty()){
                for (i in 0..ServicesModel1.size-1){
                    Services.add(ServicesModel1.get(i).id!!)
                }
                Log.e("services",Services.joinToString().replace(" ",""))
            }
            if (service!=0){
            val intent = Intent(this,RequestOrderActivity::class.java)
            intent.putExtra("services",service.toString())
            intent.putExtra("products",products)
            intent.putExtra("id",id)
                intent.putExtra("category",category)
            startActivity(intent)
            }
            else{
                CommonUtil.makeToast(mContext,getString(R.string.cleaning_type))
            }

        }
        basket.setOnClickListener {
            if (!ServicesModel1.isEmpty()){
                for (i in 0..ServicesModel1.size-1){
                    Services.add(ServicesModel1.get(i).id!!)
                }
                Log.e("services",Services.joinToString().replace(" ",""))
            }
            if (service!=0){
               AddToBasket(Gson().toJson(products),service.toString())
            }
            else{
                CommonUtil.makeToast(mContext,getString(R.string.cleaning_type))
            }
        }


    }
    fun AddToBasket(order:String,services:String){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.AddToCard(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id,order,services,0)
            ?.enqueue(object : Callback<AddOrderResponse> {
                override fun onFailure(call: Call<AddOrderResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<AddOrderResponse>,
                    response: Response<AddOrderResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            CommonUtil.makeToast(mContext,getString(R.string.order_added))
                            onBackPressed()
                            finish()
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }
}