package com.aait.cleanapp.UI.Activities

import android.content.Intent
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.Client
import com.aait.cleanapp.Models.AddOrderResponse
import com.aait.cleanapp.Models.Order
import com.aait.cleanapp.Network.Service
import com.aait.cleanapp.R
import com.aait.cleanapp.Uitls.CommonUtil
import com.bumptech.glide.Glide
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AirActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_air
    lateinit var back:ImageView
    lateinit var image:ImageView
    lateinit var name:TextView
    lateinit var add:ImageView
    lateinit var count:TextView
    lateinit var minus:ImageView
    lateinit var basket:Button
    lateinit var request:Button
    var product= 0
    var id = ""
    var photo = ""
    var na = ""
    var products = ArrayList<Order>()
    var services = ArrayList<Meter>()
    var cat = 0
    override fun initializeComponents() {
        product = intent.getIntExtra("product",0)
        id = intent.getStringExtra("id")
        cat = intent.getIntExtra("cat",0)
        photo = intent.getStringExtra("image")
        na = intent.getStringExtra("name")
        back = findViewById(R.id.back)
        image = findViewById(R.id.image)
        name = findViewById(R.id.name)
        add = findViewById(R.id.add)
        count = findViewById(R.id.count)
        minus = findViewById(R.id.minus)
        basket = findViewById(R.id.basket)
        request = findViewById(R.id.request)
        name.text = na
        services.clear()
        Glide.with(mContext).asBitmap().load(photo).into(image)
        products.clear()
        products.add(Order(cat,product.toString(),count.text.toString().toInt()))
        Log.e("product", Gson().toJson(products))
        add.setOnClickListener {
            products.clear()
            count.text = (count.text.toString().toInt()+1).toString()
            products.add(Order(cat,product.toString(),count.text.toString().toInt()))
            Log.e("product", Gson().toJson(products))
        }
        minus.setOnClickListener {
            if (count.text.toString().toInt()<=1){

            }else {
                products.clear()

                count.text = (count.text.toString().toInt() - 1).toString()
                products.add(Order(cat, product.toString(), count.text.toString().toInt()))
                Log.e("product", Gson().toJson(products))
            }
        }
        basket.setOnClickListener {
            AddToBasket(Gson().toJson(products),null)
        }

        request.setOnClickListener {
            val intent = Intent(this, RequestLocationActivity::class.java)
            intent.putExtra("services", services)
            intent.putExtra("products", products)
            intent.putExtra("id", id)
            startActivity(intent)
        }

    }
    fun AddToBasket(order:String,services:String?){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.AddToCard(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id.toInt(),order,services,1)
            ?.enqueue(object : Callback<AddOrderResponse> {
                override fun onFailure(call: Call<AddOrderResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<AddOrderResponse>,
                    response: Response<AddOrderResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            CommonUtil.makeToast(mContext,getString(R.string.order_added))
                            onBackPressed()
                            finish()
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }
}