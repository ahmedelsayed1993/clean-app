package com.aait.cleanapp.UI.Activities

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.Client
import com.aait.cleanapp.Models.Order
import com.aait.cleanapp.Models.ProductModel
import com.aait.cleanapp.Models.ServicesModel
import com.aait.cleanapp.Models.SubCategoriesResponse
import com.aait.cleanapp.Network.Service
import com.aait.cleanapp.R
import com.aait.cleanapp.UI.Adapters.CarsAdapter
import com.aait.cleanapp.Uitls.CommonUtil
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_clothes_subcats.*
import kotlinx.android.synthetic.main.recycler_cars.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CarsActivity :Parent_Activity(){
    override val layoutResource: Int
        get() = R.layout.activity_cars
    lateinit var cats:RecyclerView
    lateinit var carsAdapter: CarsAdapter
    lateinit var gridLayoutManager: GridLayoutManager
    var carsModels = ArrayList<ProductModel>()
    var productModels = ArrayList<ProductModel>()
    var id = 0
    var Services = ArrayList<ServicesModel>()
    lateinit var confirm:Button
    var category_id = 0
    lateinit var back:ImageView
    var products = ArrayList<Order>()
    var category = ""
    lateinit var text:TextView

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        category = intent.getStringExtra("category")
        cats = findViewById(R.id.cats)
        confirm = findViewById(R.id.confirm)
        gridLayoutManager = GridLayoutManager(mContext,2)
        carsAdapter = CarsAdapter(mContext,carsModels)
        cats.layoutManager = gridLayoutManager
        cats.adapter = carsAdapter
        back = findViewById(R.id.back)
        back.setOnClickListener { onBackPressed()
        finish()}
        text = findViewById(R.id.text)
        getClothes()
        products.clear()
        confirm.setOnClickListener {
            for (i in 0..carsAdapter.getData()?.size-1){
                if (carsAdapter.getData().get(i)?.count==0){

                }else{
                    products.add(Order(category_id,carsAdapter.getData().get(i)?.product_id.toString(),carsAdapter.getData().get(i)?.count))
//                    productModels.add(ProductModel(carsAdapter.getData().get(i)?.id,carsAdapter.getData().get(i)?.product_id
//                    ,carsAdapter.getData().get(i)?.count,carsAdapter.getData().get(i)?.name,carsAdapter.getData().get(i)?.image))
                }
            }
            Log.e("products",Gson().toJson(products))
            if (products.isEmpty())
            {
                CommonUtil.makeToast(mContext,getString(R.string.choose_size))
            }else{
                if (Services.isEmpty()){

                }else{
                    val intent = Intent(this,CarServicesActivity::class.java)
                    intent.putExtra("products",products)
                    intent.putExtra("services",Services)
                    intent.putExtra("category",category_id.toString())
                    intent.putExtra("id",id)
                    startActivity(intent)
                }
            }
        }


    }

    override fun onResume() {
        super.onResume()
        products.clear()
        Client.getClient()?.create(Service::class.java)?.SubCats(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id)?.enqueue(object :
            Callback<SubCategoriesResponse> {
            override fun onFailure(call: Call<SubCategoriesResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<SubCategoriesResponse>,
                response: Response<SubCategoriesResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if(response.body()?.data?.get(0)?.products!!.isEmpty()){
                            text.visibility = View.VISIBLE
                            confirm.visibility  = View.GONE
                        }else {
                            text.visibility = View.GONE
                            confirm.visibility  = View.VISIBLE
                            carsAdapter.updateAll(response.body()?.data?.get(0)?.products!!)
                            category_id = response.body()?.data?.get(0)?.subcategory_id!!
                            Services.clear()
                            for (i in 0..response.body()?.services?.size!! - 1) {
                                Services.add(
                                    ServicesModel(
                                        response.body()?.services?.get(i)?.id,
                                        response.body()?.services?.get(i)?.name,
                                        response.body()?.services?.get(i)?.checked,
                                        response.body()?.services?.get(i)?.price
                                    )
                                )
                            }
                            Log.e("services", Gson().toJson(Services))
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }

    fun getClothes(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.SubCats(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id)?.enqueue(object :
            Callback<SubCategoriesResponse> {
            override fun onFailure(call: Call<SubCategoriesResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<SubCategoriesResponse>,
                response: Response<SubCategoriesResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if(response.body()?.data?.get(0)?.products!!.isEmpty()){
                            text.visibility = View.VISIBLE
                            confirm.visibility  = View.GONE
                        }else {
                            text.visibility = View.GONE
                            confirm.visibility  = View.VISIBLE
                            carsAdapter.updateAll(response.body()?.data?.get(0)?.products!!)
                            category_id = response.body()?.data?.get(0)?.subcategory_id!!
                            Services.clear()
                            for (i in 0..response.body()?.services?.size!! - 1) {
                                Services.add(
                                    ServicesModel(
                                        response.body()?.services?.get(i)?.id,
                                        response.body()?.services?.get(i)?.name,
                                        response.body()?.services?.get(i)?.checked,
                                        response.body()?.services?.get(i)?.price
                                    )
                                )
                            }
                            Log.e("services", Gson().toJson(Services))
                        }
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
}