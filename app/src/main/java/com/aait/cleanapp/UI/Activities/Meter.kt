package com.aait.cleanapp.UI.Activities

import java.io.Serializable

class Meter:Serializable {
    var service:String?=null
    var meter:Int?=null

    constructor(service: String?, meter: Int?) {
        this.service = service
        this.meter = meter
    }
}