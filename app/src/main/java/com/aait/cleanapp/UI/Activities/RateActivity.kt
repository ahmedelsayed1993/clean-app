package com.aait.cleanapp.UI.Activities

import android.content.Intent
import android.os.Build
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.annotation.RequiresApi
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.Client
import com.aait.cleanapp.Models.AboutAppResponse
import com.aait.cleanapp.Models.NotificationModel
import com.aait.cleanapp.Network.Service
import com.aait.cleanapp.R
import com.aait.cleanapp.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RateActivity:Parent_Activity() {
    override val layoutResource: Int = R.layout.activity_rate
    lateinit var back:ImageView
    lateinit var very_sad:LinearLayout
    lateinit var sad:LinearLayout
    lateinit var acceptable:LinearLayout
    lateinit var good:LinearLayout
    lateinit var love:LinearLayout
    lateinit var very_sad_d:LinearLayout
    lateinit var sad_d:LinearLayout
    lateinit var acceptable_d:LinearLayout
    lateinit var good_d:LinearLayout
    lateinit var love_d:LinearLayout
    lateinit var provider_notes:EditText
    lateinit var delegate_notes:EditText
    lateinit var evaluate:Button
    lateinit var one:ImageView
    lateinit var two:ImageView
    lateinit var three:ImageView
    lateinit var four:ImageView
    lateinit var five:ImageView
    lateinit var six:ImageView
    lateinit var seven:ImageView
    lateinit var eight:ImageView
    lateinit var nine:ImageView
    lateinit var ten:ImageView
    var rate_pr = 1
    var rate_de = 1
   var provider = 0
    var delegate = 0
    var order = 0
    @RequiresApi(Build.VERSION_CODES.O)
    override fun initializeComponents() {
        provider = intent.getIntExtra("provider",0)
        delegate = intent.getIntExtra("delegate",0)
        order = intent.getIntExtra("order",0)
        back = findViewById(R.id.back)
        back.setOnClickListener { onBackPressed()
        finish()}
        one = findViewById(R.id.one)
        two = findViewById(R.id.two)
        three = findViewById(R.id.three)
        four = findViewById(R.id.four)
        five = findViewById(R.id.five)
        six = findViewById(R.id.six)
        seven = findViewById(R.id.seven)
        eight = findViewById(R.id.eight)
        nine = findViewById(R.id.nine)
        ten = findViewById(R.id.ten)
        love = findViewById(R.id.love)
        love_d = findViewById(R.id.love_d)
        sad = findViewById(R.id.sad)
        sad_d = findViewById(R.id.sad_d)
        very_sad = findViewById(R.id.very_sad)
        very_sad_d = findViewById(R.id.very_sad_d)
        acceptable = findViewById(R.id.acceptable)
        acceptable_d = findViewById(R.id.acceptable_d)
        good = findViewById(R.id.good)
        good_d = findViewById(R.id.good_d)
        provider_notes = findViewById(R.id.provider_notes)
        delegate_notes = findViewById(R.id.delegate_notes)
        evaluate = findViewById(R.id.evaluate)
        love_d.setOnClickListener { rate_de = 5
            var params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params.weight = 1.5f
            params.topMargin = 20
            love_d.layoutParams = params
            var params1 = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params1.weight = 1f
            params1.topMargin = 0
            acceptable_d.layoutParams = params1
            sad_d.layoutParams = params1
            very_sad_d.layoutParams = params1
            good_d.layoutParams = params1
        }
        good_d.setOnClickListener { rate_de = 4
            var params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params.weight = 1.5f
            params.topMargin = 20
            good_d.layoutParams = params
            var params1 = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params1.weight = 1f
            params1.topMargin = 0
            acceptable_d.layoutParams = params1
            sad_d.layoutParams = params1
            very_sad_d.layoutParams = params1
            love_d.layoutParams = params1
        }
        acceptable_d.setOnClickListener { rate_de = 3
            var params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params.weight = 1.5f
            params.topMargin = 20
            acceptable_d.layoutParams = params
            var params1 = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params1.weight = 1f
            params1.topMargin = 0
            good_d.layoutParams = params1
            sad_d.layoutParams = params1
            very_sad_d.layoutParams = params1
            love_d.layoutParams = params1
            }
        sad_d.setOnClickListener { rate_de = 2
            var params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params.weight = 1.5f
            params.topMargin = 20
            sad_d.layoutParams = params
            var params1 = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params1.weight = 1f
            params1.topMargin = 0
            acceptable_d.layoutParams = params1
            good_d.layoutParams = params1
            very_sad_d.layoutParams = params1
            love_d.layoutParams = params1
        }
        very_sad_d.setOnClickListener { rate_de = 1
            var params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params.weight = 1.5f
            params.topMargin = 20
            very_sad_d.layoutParams = params
            var params1 = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params1.weight = 1f
            params1.topMargin = 0
            acceptable_d.layoutParams = params1
            sad_d.layoutParams = params1
            good_d.layoutParams = params1
            love_d.layoutParams = params1
        }
        love.setOnClickListener { rate_pr = 5
            var params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params.weight = 1.5f
            params.topMargin = 20
            love.layoutParams = params
            var params1 = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params1.weight = 1f
            params1.topMargin = 0
            acceptable.layoutParams = params1
            sad.layoutParams = params1
            very_sad.layoutParams = params1
            good.layoutParams = params1}
        good.setOnClickListener { rate_pr = 4
            var params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params.weight = 1.5f
            params.topMargin = 20
            good.layoutParams = params
            var params1 = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params1.weight = 1f
            params1.topMargin = 0
            acceptable.layoutParams = params1
            sad.layoutParams = params1
            very_sad.layoutParams = params1
            love.layoutParams = params1}
        acceptable.setOnClickListener { rate_pr = 3
            var params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params.weight = 1.5f
            params.topMargin = 20
            acceptable.layoutParams = params
            var params1 = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params1.weight = 1f
            params1.topMargin = 0
            love.layoutParams = params1
            sad.layoutParams = params1
            very_sad.layoutParams = params1
            good.layoutParams = params1}
        sad.setOnClickListener { rate_pr = 2
            var params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params.weight = 1.5f
            params.topMargin = 20
            sad.layoutParams = params
            var params1 = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params1.weight = 1f
            params1.topMargin = 0
            acceptable.layoutParams = params1
            love.layoutParams = params1
            very_sad.layoutParams = params1
            good.layoutParams = params1}
        very_sad.setOnClickListener { rate_pr = 1
            var params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params.weight = 1.5f
            params.topMargin = 20
            very_sad.layoutParams = params
            var params1 = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
            params1.weight = 1f
            params1.topMargin = 0
            acceptable.layoutParams = params1
            sad.layoutParams = params1
            love.layoutParams = params1
            good.layoutParams = params1}
        evaluate.setOnClickListener {

                rate()

        }

    }

    fun ImageView.resize(
        newWidth: Int = layoutParams.width, // pixels
        newHeight: Int = layoutParams.height // pixels
    ){
        layoutParams.
            width = newWidth // in pixels
        layoutParams.height = newHeight // in pixels

    }

    fun rate(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Rate(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,
            provider,delegate,rate_pr,rate_de,provider_notes.text.toString()!!,delegate_notes.text.toString()!!,order)
            ?.enqueue(object : Callback<AboutAppResponse> {
                override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<AboutAppResponse>,
                    response: Response<AboutAppResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            CommonUtil.makeToast(mContext,response.body()?.data!!)
                            startActivity(Intent(this@RateActivity,MainActivity::class.java))
                            finish()
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }
            })
    }
}