package com.aait.cleanapp.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.cleanapp.Base.ParentRecyclerAdapter
import com.aait.cleanapp.Base.ParentRecyclerViewHolder
import com.aait.cleanapp.Models.ProductModel
import com.aait.cleanapp.Models.SubCategoriesModel
import com.aait.cleanapp.R
import com.bumptech.glide.Glide

class LocationsAdapter (context: Context, data: MutableList<SubCategoriesModel>,id:String) :
    ParentRecyclerAdapter<SubCategoriesModel>(context, data,id) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(R.layout.recycler_locations, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val bundleModel = data.get(position)
        viewHolder.name.text = bundleModel.subcategory_name
        viewHolder.productsAdapter = LocationProducrsAdapter(mcontext,viewHolder.productModels,id,bundleModel.subcategory_name!!)
        viewHolder.products.layoutManager = GridLayoutManager(mcontext,
            2)
        viewHolder.products.adapter = viewHolder.productsAdapter
        viewHolder.productsAdapter.updateAll(bundleModel.products!!)
        Glide.with(mcontext).asBitmap().load(bundleModel.subcategory_image).into(viewHolder.image)
        viewHolder.itemView.setOnClickListener(View.OnClickListener { view -> onItemClickListener.onItemClick(view,position) })



    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {




        lateinit var productsAdapter:LocationProducrsAdapter
        internal var productModels = ArrayList<ProductModel>()
        internal var name=itemView.findViewById<TextView>(R.id.name)
        internal var products = itemView.findViewById<RecyclerView>(R.id.products)
        internal var image = itemView.findViewById<ImageView>(R.id.image)


    }
}