package com.aait.cleanapp.UI.Activities

import android.content.Intent
import android.net.Uri
import android.view.Gravity
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.GravityCompat
import androidx.viewpager.widget.ViewPager
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.Client
import com.aait.cleanapp.Models.AboutAppResponse
import com.aait.cleanapp.Network.Service
import com.aait.cleanapp.R
import com.aait.cleanapp.UI.Adapters.OrdersTapAdapter
import com.aait.cleanapp.Uitls.CommonUtil
import com.bumptech.glide.Glide

import com.google.android.material.tabs.TabLayout
import com.google.firebase.iid.FirebaseInstanceId
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OrdersActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_order
    lateinit var title: TextView
    lateinit var back: ImageView
    lateinit var order: TabLayout
    lateinit var ordersViewPager: ViewPager
    private var mAdapter: OrdersTapAdapter? = null
    lateinit var menu:ImageView
    lateinit var main: LinearLayout
    lateinit var profile: LinearLayout
    lateinit var orders: LinearLayout
    lateinit var Bundles: LinearLayout
    lateinit var wallet: LinearLayout
    lateinit var about_app: LinearLayout
    lateinit var terms: LinearLayout
    lateinit var complains: LinearLayout
    lateinit var settings: LinearLayout
    lateinit var share_app: LinearLayout
    lateinit var delegate: LinearLayout
    lateinit var provioder: LinearLayout
    lateinit var name:TextView
    lateinit var image: CircleImageView
    lateinit var logout: LinearLayout

    var deviceID = ""
    override fun initializeComponents() {
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        menu = findViewById(R.id.menu)
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        back.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
        finish()}
        title.text = getString(R.string.orders)
       sideMenu()
        order = findViewById(R.id.order)
        ordersViewPager = findViewById(R.id.ordersViewPager)
        mAdapter = OrdersTapAdapter(mContext,supportFragmentManager)
        ordersViewPager.setAdapter(mAdapter)
        order!!.setupWithViewPager(ordersViewPager)

    }
    fun sideMenu(){
        drawer_layout.useCustomBehavior(Gravity.START)
        drawer_layout.useCustomBehavior(Gravity.END)
        drawer_layout.setRadius(Gravity.START, 80f)
        drawer_layout.setRadius(Gravity.END, 80f)
        drawer_layout.setViewScale(Gravity.START, 0.7f) //set height scale for main view (0f to 1f)
        drawer_layout.setViewScale(Gravity.END, 0.7f) //set height scale for main view (0f to 1f)
        drawer_layout.setViewElevation(Gravity.START, 0f) //set main view elevation when drawer open (dimension)
        drawer_layout.setViewElevation(Gravity.END, 0f) //set main view elevation when drawer open (dimension)
//        drawer_layout.setViewScrimColor(Gravity.START, Color.) //set drawer overlay coloe (color)
//        drawer_layout.setViewScrimColor(Gravity.END, Color.TRANSPARENT) //set drawer overlay coloe (color)
        drawer_layout.setDrawerElevation(Gravity.START, 80f) //set drawer elevation (dimension)
        drawer_layout.setDrawerElevation(Gravity.END, 80f) //set drawer elevation (dimension)
        drawer_layout.setContrastThreshold(3f) //set maximum of contrast ratio between white text and background color.
        drawer_layout.setRadius(Gravity.START, 0f) //set end container's corner radius (dimension)

        drawer_layout.setRadius(Gravity.END, 0f)

        menu.setOnClickListener { drawer_layout.openDrawer(GravityCompat.START) }

        name = drawer_layout.findViewById(R.id.name)
        image = drawer_layout.findViewById(R.id.image)
        main = drawer_layout.findViewById(R.id.main)
        profile = drawer_layout.findViewById(R.id.profile)
        orders = drawer_layout.findViewById(R.id.orders)
        delegate = drawer_layout.findViewById(R.id.delegate)
        Bundles = drawer_layout.findViewById(R.id.Bundles)
        wallet = drawer_layout.findViewById(R.id.wallet)
        provioder = drawer_layout.findViewById(R.id.provider)
        about_app = drawer_layout.findViewById(R.id.about_app)
        terms = drawer_layout.findViewById(R.id.terms)
        complains = drawer_layout.findViewById(R.id.complains)
        settings = drawer_layout.findViewById(R.id.settings)
        share_app = drawer_layout.findViewById(R.id.share_app)
        logout = drawer_layout.findViewById(R.id.logout)
        main.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
            this.finish()}
        name.text = mSharedPrefManager.userData.name
        Glide.with(mContext).asBitmap().load(mSharedPrefManager.userData.avatar).into(image)
        delegate.setOnClickListener {  startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://play.google.com/store/apps/details?id=com.aait.cleanappprovider")
            )
        ) }
        profile.setOnClickListener { startActivity(Intent(this,ProfileActivity::class.java)) }
        provioder.setOnClickListener {   startActivity(
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse("https://play.google.com/store/apps/details?id=com.aait.cleanappprovider")
            )
        )}
        settings.setOnClickListener { startActivity(Intent(this,SettingsActivity::class.java)) }
        about_app.setOnClickListener { startActivity(Intent(this,AboutAppActivity::class.java)) }
        terms.setOnClickListener { startActivity(Intent(this,TermsActivity::class.java)) }
        share_app.setOnClickListener { CommonUtil.ShareApp(mContext) }
        orders.setOnClickListener { drawer_layout.closeDrawer(GravityCompat.START) }
        complains.setOnClickListener { startActivity(Intent(this,ComplainsActivity::class.java)) }
        Bundles.setOnClickListener { startActivity(Intent(this,BundleActivity::class.java)) }
        wallet.setOnClickListener { startActivity(Intent(this,WalletActivity::class.java)) }
        logout.setOnClickListener { logout() }
    }
    fun logout(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.logOut(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,"android",deviceID)?.enqueue(object :
            Callback<AboutAppResponse> {
            override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {

                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<AboutAppResponse>,
                response: Response<AboutAppResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        mSharedPrefManager.loginStatus=false
                        mSharedPrefManager.Logout()
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        startActivity(Intent(this@OrdersActivity, SplashActivity::class.java))
                        finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
}