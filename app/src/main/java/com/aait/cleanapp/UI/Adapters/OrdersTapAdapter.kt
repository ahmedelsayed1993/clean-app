package com.aait.cleanapp.UI.Adapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.aait.cleanapp.R
import com.aait.cleanapp.UI.Fragments.*

class OrdersTapAdapter(
    private val context: Context,
    fm: FragmentManager
) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        return if (position == 0) {
            CurrentFragment()
        }else if (position == 1){
            UnderWayFragment()
        }
        else{
            PreviousFragment()
        }
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (position == 0) {
            context.getString(R.string.the_current_orders)
        }else if(position == 1){
            context.getString(R.string.Waiting_for_delivery)
        }
        else{
            context.getString(R.string.Previous_orders)
        }
    }
}
