package com.aait.cleanapp.UI.Activities

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.Client
import com.aait.cleanapp.Listeners.OnItemClickListener
import com.aait.cleanapp.Models.*
import com.aait.cleanapp.Network.Service
import com.aait.cleanapp.R
import com.aait.cleanapp.UI.Adapters.CardClothesAdapter
import com.aait.cleanapp.UI.Adapters.CardServicesAdapter
import com.aait.cleanapp.Uitls.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CardDetailsClothesActivity:Parent_Activity(),OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {

        if (view.id == R.id.service){
            if (cardServicesModels.get(position).checked==0){
                cardServicesModels.get(position).checked = 1
                Services.add(cardServicesModels.get(position)?.id!!)
                cardServicesAdapter.notifyDataSetChanged()
                Log.e("list", Gson().toJson(Services))
            }else{
                cardServicesModels.get(position).checked = 0
                Log.e("position",position.toString())
                if (Services.size>1) {
                    for (i in 0..Services.size - 1){

                        if (Services.get(i)==cardServicesModels.get(position)?.id){
                           // var list = ListModel(ServicesModel1.get(i).id,ServicesModel1.get(i).name)
                            Services.removeAt(
                                i
                            )
                            break
                        }
                    }

                }else if (Services.size==1){
                    Services.clear()
                }

                cardServicesAdapter.notifyDataSetChanged()
                Log.e("list1", Gson().toJson(Services))
            }
        }
    }

    override val layoutResource: Int
        get() = R.layout.activity_card_details_clothes
    lateinit var back:ImageView
    lateinit var products:RecyclerView
    lateinit var services:RecyclerView
    lateinit var vat:TextView
    lateinit var vat_price:TextView
    lateinit var delivery_price:TextView
    lateinit var total:TextView
    lateinit var confirm:Button
    lateinit var cardClothesAdapter: CardClothesAdapter
    lateinit var cardServicesAdapter: CardServicesAdapter
    var cardOrdermodels = ArrayList<CardOrderModel>()
    var cardServicesModels = ArrayList<CardServicesModel>()
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var gridLayoutManager: GridLayoutManager
    var id = 0
    var product = ArrayList<Order>()
    var Services = ArrayList<Int>()

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        back = findViewById(R.id.back)
        products = findViewById(R.id.products)
        services = findViewById(R.id.services)
        vat = findViewById(R.id.vat)
        vat_price = findViewById(R.id.vat_price)
        delivery_price = findViewById(R.id.delivery_price)
        total = findViewById(R.id.total)
        confirm = findViewById(R.id.confirm)
        back.setOnClickListener { onBackPressed()
        finish()}
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        gridLayoutManager = GridLayoutManager(mContext,2)
        cardClothesAdapter = CardClothesAdapter(mContext,cardOrdermodels)
        cardServicesAdapter = CardServicesAdapter(mContext,cardServicesModels,R.layout.recycler_services)
        cardClothesAdapter.setOnItemClickListener(this)
        cardServicesAdapter.setOnItemClickListener(this)
        products.layoutManager = linearLayoutManager
        services.layoutManager = gridLayoutManager
        products.adapter = cardClothesAdapter
        services.adapter = cardServicesAdapter
        getData()
        product.clear()
        confirm.setOnClickListener {
            for (i in 0..cardClothesAdapter.getData()?.size - 1) {
                for (j in 0..cardClothesAdapter.getData()?.get(i)?.products?.size!! - 1) {
                    if (!cardClothesAdapter.getData()?.get(i)?.products?.get(j)?.count!!.equals("0")) {
                        product.add(
                            Order(
                                cardClothesAdapter.getData()?.get(i).subcategory,
                                cardClothesAdapter.getData()?.get(i)?.products?.get(j)?.id.toString()
                                ,
                                cardClothesAdapter.getData()?.get(i)?.products?.get(j)?.count!!.toInt()
                            )
                        )

                    }

                }
            }
            Log.e("product",Gson().toJson(product))
            Log.e("service",Services.joinToString().replace(" ",""))
            if (product.isEmpty()) {
                CommonUtil.makeToast(mContext, getString(R.string.choose_clothes))
            } else {
                if (Services.isEmpty()) {
                    CommonUtil.makeToast(mContext, getString(R.string.choose_service))
                } else {
                    val intent = Intent(this, UpdateOrderActivity::class.java)
                    intent.putExtra("services", Services.joinToString().replace(" ", ""))
                    intent.putExtra("products", product)
                    intent.putExtra("id", id)
                    intent.putExtra("category","clothes")
                    startActivity(intent)
                }
            }
        }


    }

    override fun onResume() {
        super.onResume()
        product.clear()
    }

    fun getData(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getCardDetails(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id)
            ?.enqueue(object : Callback<CardDetailsOrderResponse> {
                override fun onFailure(call: Call<CardDetailsOrderResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<CardDetailsOrderResponse>,
                    response: Response<CardDetailsOrderResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            cardServicesAdapter.updateAll(response.body()?.services!!)
                            cardClothesAdapter.updateAll(response.body()?.data!!)
                            for (i in 0..response.body()?.services?.size!!-1){
                                if (response.body()?.services?.get(i)?.checked!! !=0) {
                                    Services.add(response.body()?.services?.get(i)?.id!!)
                                }
                            }
//                            vat.text = response.body()?.tax+" % "
//                            vat_price.text = response.body()?.total_tax +" "+ getString(R.string.rs)
                          //  total.text = response.body()?.total +" "+getString(R.string.rs)
//                            delivery_price.text = response.body()?.delivery +" "+getString(R.string.rs)
                            if (response.body()?.delivery!!.equals(""))
                            {
                                delivery_price.text = getString(R.string.not_detected)
                            }else{
                                delivery_price.text = response.body()?.delivery +" "+getString(R.string.rs)
                            }
                            if (response.body()?.total_tax!!.equals(""))
                            {
                                vat_price.text = getString(R.string.not_detected)
                            }else{
                                vat_price.text = response.body()?.total_tax +" "+getString(R.string.rs)
                            }
                            if (response.body()?.tax!!.equals(""))
                            {
                                vat.text = getString(R.string.not_detected)
                            }else{
                                vat.text = response.body()?.tax +" %"
                            }
                            if (response.body()?.total!!.equals(""))
                            {
                                total.text = getString(R.string.not_detected)
                            }else{
                                total.text = response.body()?.total +" "+getString(R.string.rs)
                            }
                        }
                    }
                }

            })
    }


}