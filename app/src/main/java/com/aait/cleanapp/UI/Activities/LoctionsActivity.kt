package com.aait.cleanapp.UI.Activities

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.Client
import com.aait.cleanapp.Listeners.OnItemClickListener
import com.aait.cleanapp.Models.SubCategoriesModel
import com.aait.cleanapp.Models.SubCategoriesResponse
import com.aait.cleanapp.Network.Service
import com.aait.cleanapp.R
import com.aait.cleanapp.UI.Adapters.ClothesAdapter
import com.aait.cleanapp.UI.Adapters.LocationsAdapter
import com.aait.cleanapp.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoctionsActivity:Parent_Activity(),OnItemClickListener {
    override fun onItemClick(view: View, position: Int) {

    }

    override val layoutResource: Int
        get() = R.layout.activity_locations
    lateinit var cats: RecyclerView
    lateinit var locationAdapter: LocationsAdapter
    lateinit var linearLayoutManager: LinearLayoutManager
    var subCategoriesModels = ArrayList<SubCategoriesModel>()
    var id = 0
    lateinit var back:ImageView
    lateinit var text:TextView

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        cats = findViewById(R.id.cats)
        back = findViewById(R.id.back)
        back.setOnClickListener { onBackPressed()
        finish()}
        text = findViewById(R.id.text)
        locationAdapter = LocationsAdapter(mContext,subCategoriesModels,id.toString())
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        locationAdapter.setOnItemClickListener(this)
        cats.layoutManager = linearLayoutManager
        cats.adapter = locationAdapter
        getClothes()
    }
    fun getClothes(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.SubCats(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id)?.enqueue(object :
            Callback<SubCategoriesResponse> {
            override fun onFailure(call: Call<SubCategoriesResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<SubCategoriesResponse>,
                response: Response<SubCategoriesResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        if (response.body()?.data!!.isEmpty()){
                            text.visibility = View.VISIBLE
                        }else {
                            text.visibility = View.GONE
                            locationAdapter.updateAll(response.body()?.data!!)
                        }

                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
}