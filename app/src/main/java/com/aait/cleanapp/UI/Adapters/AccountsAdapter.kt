package com.aait.cleanapp.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import com.aait.cleanapp.Base.ParentRecyclerAdapter
import com.aait.cleanapp.Base.ParentRecyclerViewHolder
import com.aait.cleanapp.Models.AccountsModel
import com.aait.cleanapp.Models.BundleModel
import com.aait.cleanapp.R

class AccountsAdapter (context: Context, data: MutableList<AccountsModel>, layoutId: Int) :
    ParentRecyclerAdapter<AccountsModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val bundleModel = data.get(position)
        viewHolder.bank_name.text = bundleModel.bank_name
        viewHolder.account_name.text = bundleModel.account_name
        viewHolder.account_number.text = bundleModel.account_number
        viewHolder.iban_name.text = bundleModel.iban_number


    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {





        internal var bank_name=itemView.findViewById<TextView>(R.id.bank_name)
        internal var account_name = itemView.findViewById<TextView>(R.id.account_name)
        internal var account_number = itemView.findViewById<TextView>(R.id.account_number)
        internal var iban_name = itemView.findViewById<TextView>(R.id.iban_name)


    }
}