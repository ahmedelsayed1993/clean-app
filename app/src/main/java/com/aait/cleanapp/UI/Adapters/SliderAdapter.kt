package com.aait.cleanapp.UI.Adapters

import android.content.Context

import android.view.View
import com.aait.cleanapp.Models.BannersModel
import com.aait.cleanapp.R

import com.github.islamkhsh.CardSliderAdapter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.card_image_slider.view.*

class SliderAdapter (context:Context,list : ArrayList<BannersModel>) : CardSliderAdapter<BannersModel>(list) {
    var list = list
    var context=context
    override fun bindView(position: Int, itemContentView: View, item: BannersModel?) {

            Picasso.with(context).load(item?.image).resize(300,300).into(itemContentView.image)
            itemContentView.setOnClickListener {

            }

    }

    override fun getItemContentLayout(position: Int) : Int { return R.layout.card_image_slider }
}