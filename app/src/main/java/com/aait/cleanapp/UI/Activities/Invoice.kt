package com.aait.cleanapp.UI.Activities

import android.content.Intent
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.Client
import com.aait.cleanapp.Models.CodeResponse
import com.aait.cleanapp.Models.InvoiceResponse
import com.aait.cleanapp.Models.ItemInvoiceModel
import com.aait.cleanapp.Network.Service
import com.aait.cleanapp.R
import com.aait.cleanapp.UI.Adapters.InvoiceAdapter
import com.aait.cleanapp.Uitls.CommonUtil
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Invoice: Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.invoice

    lateinit var back: ImageView
    lateinit var title: TextView
    lateinit var items: RecyclerView
    lateinit var vat: TextView
    lateinit var vat_price: TextView
    lateinit var delivery_price: TextView
    lateinit var total: TextView
    var itemInvoiceModels = ArrayList<ItemInvoiceModel>()
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var invoiceAdapter: InvoiceAdapter

    lateinit var delivery: LinearLayout

    var id = 0
    var category = ""
    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        category = intent.getStringExtra("category")
        back = findViewById(R.id.back)
        title = findViewById(R.id.title)
        items = findViewById(R.id.items)
        vat = findViewById(R.id.vat)

        vat_price = findViewById(R.id.vat_price)
        delivery = findViewById(R.id.delivery)
        delivery_price = findViewById(R.id.delivery_price)
        total = findViewById(R.id.total)

        title.text = getString(R.string.invoice)
        back.setOnClickListener { onBackPressed()
            finish()}
        linearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL,false)
        invoiceAdapter = InvoiceAdapter(mContext,itemInvoiceModels, R.layout.recycler_invoice)
        items.layoutManager = linearLayoutManager
        items.adapter = invoiceAdapter
        back.setOnClickListener { onBackPressed()
            finish()}
        if (category.equals("clothes")){
            delivery.visibility = View.VISIBLE
        }else{
            delivery.visibility = View.GONE
        }
        Invoice()



    }

    fun Invoice(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.getInvoice(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id)?.enqueue(
            object : Callback<InvoiceResponse> {
                override fun onFailure(call: Call<InvoiceResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<InvoiceResponse>,
                    response: Response<InvoiceResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            vat.text = response.body()?.tax+" % "
                            vat_price.text = response.body()?.total_tax +" "+ getString(R.string.rs)
                            total.text = response.body()?.total +" "+getString(R.string.rs)
                            delivery_price.text = response.body()?.delivery +" "+getString(R.string.rs)
                            invoiceAdapter.updateAll(response.body()?.data!!)
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            }
        )
    }

}