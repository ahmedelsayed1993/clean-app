package com.aait.cleanapp.UI.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.aait.cleanapp.Base.ParentRecyclerAdapter
import com.aait.cleanapp.Base.ParentRecyclerViewHolder
import com.aait.cleanapp.Models.NotificationModel
import com.aait.cleanapp.Models.OfferModel
import com.aait.cleanapp.R
import com.daimajia.swipe.SwipeLayout

class OffersAdapter (context: Context, data: MutableList<OfferModel>, layoutId: Int) :
    ParentRecyclerAdapter<OfferModel>(context, data, layoutId) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ParentRecyclerViewHolder {
        val itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false)
        return ViewHolder(itemView)
    }
    var TextView.textColor: Int
        get() = currentTextColor
        set(v) = setTextColor(v)

    override fun onBindViewHolder(holder: ParentRecyclerViewHolder, position: Int) {
        val viewHolder = holder as ViewHolder
        val listModel = data.get(position)
        viewHolder.title!!.setText(listModel.content)




    }
    inner class ViewHolder internal constructor(itemView: View) :
        ParentRecyclerViewHolder(itemView) {

        internal var title=itemView.findViewById<TextView>(R.id.title)

    }
}