package com.aait.cleanapp.UI.Activities

import android.content.Intent
import android.os.Build
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.Client
import com.aait.cleanapp.Models.AboutAppResponse
import com.aait.cleanapp.Network.Service
import com.aait.cleanapp.R
import com.aait.cleanapp.Uitls.CommonUtil
import com.aait.cleanapp.Uitls.PermissionUtils
import com.bumptech.glide.Glide
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.fxn.utility.ImageQuality
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File

class BankTransferActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_bank_transfer
    lateinit var menu:ImageView
    lateinit var title:TextView
    lateinit var back:ImageView
    lateinit var owner_name:EditText
    lateinit var bank_name:EditText
    lateinit var amount:TextView
    lateinit var add_image:ImageView
    lateinit var send:Button
    var price = 0
    internal var returnValue: java.util.ArrayList<String>? = java.util.ArrayList()
    internal var options = Options.init()
        .setRequestCode(100)                                                 //Request code for activity results
        .setCount(1)                                                         //Number of images to restict selection count
        .setFrontfacing(false)                                                //Front Facing camera on start
        .setImageQuality(ImageQuality.HIGH)                                  //Image Quality
        .setPreSelectedUrls(returnValue)                                     //Pre selected Image Urls
        .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT)           //Orientaion
        .setPath("/pix/images")
    private var ImageBasePath: String? = null

    override fun initializeComponents() {
        price = intent.getIntExtra("price",0)
        menu = findViewById(R.id.menu)
        title = findViewById(R.id.title)
        back = findViewById(R.id.back)
        owner_name = findViewById(R.id.owner_name)
        bank_name = findViewById(R.id.bank_name)
        amount = findViewById(R.id.amount_paid)
        add_image = findViewById(R.id.add_image)
        send = findViewById(R.id.send)
        menu.visibility = View.GONE
        title.text = getString(R.string.bank_accounts)
        back.setOnClickListener { onBackPressed()
        finish()}
        amount.setText(price.toString())
        add_image.setOnClickListener {
            if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
                if (!(PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.CAMERA)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE)&& PermissionUtils.hasPermissions(mContext,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            )) {
                    CommonUtil.PrintLogE("Permission not granted")
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(
                            PermissionUtils.IMAGE_PERMISSIONS,
                            400
                        )
                    }
                } else {
                    Pix.start(this, options)
                    CommonUtil.PrintLogE("Permission is granted before")
                }
            } else {
                CommonUtil.PrintLogE("SDK minimum than 23")
                Pix.start(this, options)
            }
        }

        send.setOnClickListener {
            if (CommonUtil.checkEditError(owner_name,getString(R.string.enter_owner_name))||
                    CommonUtil.checkEditError(bank_name,getString(R.string.enter_bank_name))||
                    CommonUtil.checkTextError(amount,getString(R.string.enter_amount))){
                return@setOnClickListener
            }else{
                if (ImageBasePath!=null){
                    Transfer(ImageBasePath!!)
                }else{
                    CommonUtil.makeToast(mContext,getString(R.string.add_image))
                }
            }
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            if (resultCode==0){

            }else {
                returnValue = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)

                ImageBasePath = returnValue!![0]

                // Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.upload).fitCenter().sizeMultiplier(100)).into(profile_image);

                if (ImageBasePath != null) {

                    Glide.with(mContext).asBitmap().load(ImageBasePath).into(add_image)
                }
            }
        }
    }

    fun Transfer(path:String){
        showProgressDialog(getString(R.string.please_wait))
        var filePart: MultipartBody.Part? = null
        val ImageFile = File(path)
        val fileBody = RequestBody.create(MediaType.parse("*/*"), ImageFile)
        filePart = MultipartBody.Part.createFormData("image", ImageFile.name, fileBody)
        Client.getClient()?.create(Service::class.java)?.BankTransfer(mSharedPrefManager.userData.id!!,mLanguagePrefManager.appLanguage,owner_name.text.toString()
        ,bank_name.text.toString(),amount.text.toString(),filePart)?.enqueue(object :
            Callback<AboutAppResponse> {
            override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(
                call: Call<AboutAppResponse>,
                response: Response<AboutAppResponse>
            ) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        CommonUtil.makeToast(mContext,response.body()?.data!!)
                        startActivity(Intent(this@BankTransferActivity,MainActivity::class.java))
                        this@BankTransferActivity.finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }
        })
    }
}