package com.aait.cleanapp.UI.Activities

import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.Client
import com.aait.cleanapp.Listeners.OnItemClickListener
import com.aait.cleanapp.Models.AddOrderResponse
import com.aait.cleanapp.Models.Order
import com.aait.cleanapp.Models.ServicesModel
import com.aait.cleanapp.Models.ServicesResponse
import com.aait.cleanapp.Network.Service
import com.aait.cleanapp.R
import com.aait.cleanapp.UI.Adapters.CarServicesAdapter
import com.aait.cleanapp.UI.Adapters.LocationServicesAdapter
import com.aait.cleanapp.Uitls.CommonUtil
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LocationsServicesActivity :Parent_Activity(),OnItemClickListener{
    override fun onItemClick(view: View, position: Int) {
        if (view.id == R.id.name) {

            if (locationServicesAdapter.getData().get(position).checked == 1) {
                locationServicesAdapter.getData().get(position).checked = 0
                Services.clear()
                locationServicesAdapter.notifyDataSetChanged()
                Log.e("services", Gson().toJson(Services))

            } else {
                Services.clear()
                locationServicesAdapter.getData().get(position).checked = 1
                locationServicesAdapter.selected = position
                if (locationServicesAdapter.getData().get(position)?.count.equals("")){
                    CommonUtil.makeToast(mContext,getString(R.string.enter_number_of_meters))
                }else {
                    model = Meter(locationServicesAdapter.getData()?.get(position)?.id.toString(),
                        locationServicesAdapter.getData()?.get(position)?.count?.toInt())
                    Services.add(
                        Meter(
                            locationServicesAdapter.getData()?.get(position)?.id.toString(),
                            locationServicesAdapter.getData()?.get(position)?.count?.toInt()
                        )
                    )
                }
                locationServicesAdapter.notifyDataSetChanged()
                Log.e("services", Gson().toJson(Services))
               // Log.e("meter",Gson().toJson(model))

            }
           // locationServicesAdapter.notifyDataSetChanged()
        }

    }

    override val layoutResource: Int
        get() = R.layout.activity_locations_services

    var id = ""
    var product = 0
    var order = ArrayList<Order>()
    lateinit var cats:RecyclerView
    lateinit var linearLayoutManager: LinearLayoutManager
    lateinit var locationServicesAdapter: LocationServicesAdapter
    var servicesModels = ArrayList<ServicesModel>()
    var Services = ArrayList<Meter>(1)
    lateinit var request:Button
    lateinit var basket:Button
    lateinit var model:Meter
    lateinit var back:ImageView
    override fun initializeComponents() {
        id = intent.getStringExtra("id")
        product = intent.getIntExtra("product",0)
        order = intent.getSerializableExtra("order") as ArrayList<Order>
        cats = findViewById(R.id.cats)
        request = findViewById(R.id.request)
        basket = findViewById(R.id.basket)
        back = findViewById(R.id.back)
        back.setOnClickListener { onBackPressed()
        finish()}
        linearLayoutManager = LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false)
        locationServicesAdapter = LocationServicesAdapter(mContext,servicesModels,R.layout.recycler_location_services)
        locationServicesAdapter.setOnItemClickListener(this)
        cats.layoutManager = linearLayoutManager
        cats.adapter = locationServicesAdapter
        getServices()
        Services.takeLast(1)
        request.setOnClickListener {
            Log.e("order",Gson().toJson(order))
            Log.e("service",Gson().toJson(Services))
            for (i in 0..locationServicesAdapter.getData()?.size-1){
                if (locationServicesAdapter.getData()?.get(i)?.checked==1){
                    if (!locationServicesAdapter.getData()?.get(i)?.count.equals("")){
                        if (!locationServicesAdapter.getData()?.get(i)?.count.equals("0")) {
                            Services.clear()
                            Services.add(
                                Meter(
                                    locationServicesAdapter.getData()?.get(i)?.id.toString(),
                                    locationServicesAdapter.getData()?.get(i)?.count!!.toInt()
                                )
                            )
                        }}
                }
            }
            Log.e("services",Gson().toJson(Services))
            if (Services.isEmpty()){
                CommonUtil.makeToast(mContext,getString(R.string.enter_number_of_meters))
            }else {
                if (Services.get(0).meter==0 ){
                    CommonUtil.makeToast(mContext,getString(R.string.enter_number_of_meters))
                }else {
                    val intent = Intent(this, RequestLocationActivity::class.java)
                    intent.putExtra("services", Services)
                    intent.putExtra("products", order)
                    intent.putExtra("id", id)
                    startActivity(intent)
                }
            }

        }
        basket.setOnClickListener {
            for (i in 0..locationServicesAdapter.getData()?.size-1){
                if (locationServicesAdapter.getData()?.get(i)?.checked==1){
                    if (!locationServicesAdapter.getData()?.get(i)?.count.equals("")){
                        Services.clear()
                        Services.add(Meter(locationServicesAdapter.getData()?.get(i)?.id.toString(),locationServicesAdapter.getData()?.get(i)?.count!!.toInt()))
                    }
                }
            }
            Log.e("services",Gson().toJson(Services))
            if (Services.isEmpty()){
                CommonUtil.makeToast(mContext,getString(R.string.enter_number_of_meters))
            }else {
                if (Services.get(0).meter==0 ){
                    CommonUtil.makeToast(mContext,getString(R.string.enter_number_of_meters))
                }else {
                    AddToBasket(Gson().toJson(order), Gson().toJson(Services))
                }
            }
        }

    }

    fun getServices(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.ProductServices(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,product)?.enqueue(
            object : Callback<ServicesResponse> {
                override fun onFailure(call: Call<ServicesResponse>, t: Throwable) {
                    hideProgressDialog()
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                }

                override fun onResponse(
                    call: Call<ServicesResponse>,
                    response: Response<ServicesResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            if(response.body()?.data?.isEmpty()!!){

                            }else {
                                locationServicesAdapter.updateAll(response.body()?.data!!)
                                Services.clear()
                                Services.add(
                                    Meter(
                                        locationServicesAdapter.getData().get(0).id.toString(),
                                        0
                                    )
                                )
                                locationServicesAdapter.getData().get(0).checked = 1
                                Log.e("services", Gson().toJson(Services))
                            }
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            }
        )
    }
    fun AddToBasket(order:String,services:String){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.AddToCard(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id.toInt(),order,services,1)
            ?.enqueue(object :Callback<AddOrderResponse>{
                override fun onFailure(call: Call<AddOrderResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<AddOrderResponse>,
                    response: Response<AddOrderResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            CommonUtil.makeToast(mContext,getString(R.string.order_added))
                            onBackPressed()
                            finish()
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            })
    }
}