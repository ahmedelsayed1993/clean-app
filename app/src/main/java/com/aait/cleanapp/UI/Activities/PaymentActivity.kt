package com.aait.cleanapp.UI.Activities

import android.content.Intent
import android.widget.Button
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.Client
import com.aait.cleanapp.Models.AboutAppResponse
import com.aait.cleanapp.Models.BaseResponse
import com.aait.cleanapp.Network.Service
import com.aait.cleanapp.R
import com.aait.cleanapp.Uitls.CommonUtil
import kotlinx.android.synthetic.main.activity_card_details_cars.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PaymentActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_payment
    lateinit var back:ImageView
    lateinit var card:RadioButton
    lateinit var visa:RadioButton
    lateinit var mada:RadioButton
    lateinit var wallet:RadioButton
    lateinit var cash:RadioButton
    lateinit var next:Button
    lateinit var show_bill:Button
    lateinit var title:TextView
    var id = 0
    var payment = "cash"
    var category = ""

    override fun initializeComponents() {
        id = intent.getIntExtra("id",0)
        category = intent.getStringExtra("category")
        title = findViewById(R.id.title)
        title.text = getString(R.string.order_num)+id.toString()
        back = findViewById(R.id.back)
        card = findViewById(R.id.card)
        visa = findViewById(R.id.visa)
        mada = findViewById(R.id.mada)
        wallet = findViewById(R.id.wallet)
        cash = findViewById(R.id.cash)
        next = findViewById(R.id.next)
        show_bill = findViewById(R.id.show_bill)
        back.setOnClickListener { startActivity(Intent(this,MainActivity::class.java))
        finish()}
        card.isActivated=false
        visa.isActivated=false
        mada.isActivated=false
        card.setOnClickListener { payment = "online" }
        visa.setOnClickListener { payment = "online" }
        mada.setOnClickListener { payment = "online" }
        wallet.setOnClickListener { payment = "balance" }
        cash.setOnClickListener { payment = "cash" }
        next.setOnClickListener { Pay() }
        show_bill.setOnClickListener {  val intent = Intent(this,Invoice::class.java)
            intent.putExtra("id",id)
            intent.putExtra("category",category)
            startActivity(intent) }



    }

    override fun onBackPressed() {
        super.onBackPressed()

        finish()
    }
    fun Pay(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.Pay(mLanguagePrefManager.appLanguage,mSharedPrefManager.userData.id!!,id,payment)?.enqueue(
            object : Callback<AboutAppResponse>{
                override fun onFailure(call: Call<AboutAppResponse>, t: Throwable) {
                    CommonUtil.handleException(mContext,t)
                    t.printStackTrace()
                    hideProgressDialog()
                }

                override fun onResponse(
                    call: Call<AboutAppResponse>,
                    response: Response<AboutAppResponse>
                ) {
                    hideProgressDialog()
                    if (response.isSuccessful){
                        if (response.body()?.value.equals("1")){
                            CommonUtil.makeToast(mContext,response.body()?.data!!)
                            val intent = Intent(this@PaymentActivity,BackHomeActivity::class.java)
                            intent.putExtra("id",id)
                            startActivity(intent)
                            finish()
                        }else{
                            CommonUtil.makeToast(mContext,response.body()?.msg!!)
                        }
                    }
                }

            }
        )
    }
}