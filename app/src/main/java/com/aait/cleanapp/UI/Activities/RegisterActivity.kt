package com.aait.cleanapp.UI.Activities

import android.content.Intent
import android.provider.ContactsContract
import android.text.InputType
import android.widget.*
import com.aait.cleanapp.Base.Parent_Activity
import com.aait.cleanapp.Client
import com.aait.cleanapp.Models.UserResponse
import com.aait.cleanapp.Network.Service
import com.aait.cleanapp.R
import com.aait.cleanapp.Uitls.CommonUtil
import com.google.firebase.iid.FirebaseInstanceId
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.microedition.khronos.egl.EGLDisplay

class RegisterActivity:Parent_Activity() {
    override val layoutResource: Int
        get() = R.layout.activity_register
    lateinit var mobile:EditText
    lateinit var name:EditText
    lateinit var location:TextView
    lateinit var address:EditText
    lateinit var password:EditText
    lateinit var view:ImageView
    lateinit var confirm:EditText
    lateinit var view_confirm:ImageView
    lateinit var register:Button
    lateinit var login:LinearLayout
    var lat = ""
    var lng = ""
    var result = ""

    var deviceID = ""
    override fun initializeComponents() {
        deviceID = FirebaseInstanceId.getInstance().token.toString()
        mobile = findViewById(R.id.mobile)
        name = findViewById(R.id.name)
        location = findViewById(R.id.location)
        address = findViewById(R.id.address)
        password = findViewById(R.id.password)
        view = findViewById(R.id.view)
        confirm = findViewById(R.id.confirm)
        view_confirm = findViewById(R.id.view_confirm)
        register = findViewById(R.id.register)
        login = findViewById(R.id.login)
        view.setOnClickListener {  if (password.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL){
            password.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        }
        else{
            password.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL
        } }
        view_confirm.setOnClickListener {
            if (confirm.inputType== InputType.TYPE_TEXT_VARIATION_NORMAL){
                confirm.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

            }
            else{
                confirm.inputType = InputType.TYPE_TEXT_VARIATION_NORMAL
            }
        }

        location.setOnClickListener {
            startActivityForResult(Intent(this@RegisterActivity,LocationActivity::class.java),1)
        }
        login.setOnClickListener { startActivity(Intent(this,LoginActivity::class.java))
        finish()}

        register.setOnClickListener {
            if (CommonUtil.checkEditError(mobile,getString(R.string.enter_phone))||
                    CommonUtil.checkLength(mobile,getString(R.string.phone_length),9)||
                    CommonUtil.checkEditError(name,getString(R.string.enter_name))||
                    CommonUtil.checkTextError(location,getString(R.string.enter_location))||
                    CommonUtil.checkEditError(address,getString(R.string.Please_specify_the_address))||
                    CommonUtil.checkEditError(password,getString(R.string.enter_password))||
                    CommonUtil.checkLength(password,getString(R.string.password_length),6)||
                    CommonUtil.checkEditError(confirm,getString(R.string.enter_password))){
                return@setOnClickListener
            }else{
                if (!password.text.toString().equals(confirm.text.toString())){
                    confirm.error = getString(R.string.password_not_match)
                }else{
                       Register()
                }
            }
        }


    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data?.getStringExtra("result")!=null) {
            result = data?.getStringExtra("result").toString()
            lat = data?.getStringExtra("lat").toString()
            lng = data?.getStringExtra("lng").toString()
            location.text = result
        }else{
            result = ""
            lat = ""
            lng = ""
            location.text = ""
        }
    }

    fun Register(){
        showProgressDialog(getString(R.string.please_wait))
        Client.getClient()?.create(Service::class.java)?.SignUp(mobile.text.toString(),name.text.toString(),lat,lng,address.text.toString(),password.text.toString(),
        deviceID,"android",mLanguagePrefManager.appLanguage)?.enqueue(object : Callback<UserResponse>{
            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                CommonUtil.handleException(mContext,t)
                t.printStackTrace()
                hideProgressDialog()
            }

            override fun onResponse(call: Call<UserResponse>, response: Response<UserResponse>) {
                hideProgressDialog()
                if (response.isSuccessful){
                    if (response.body()?.value.equals("1")){
                        val intent = Intent(this@RegisterActivity,AcivateActivity::class.java)
                        intent.putExtra("user",response.body()?.data)
                        startActivity(intent)
                        this@RegisterActivity.finish()
                    }else{
                        CommonUtil.makeToast(mContext,response.body()?.msg!!)
                    }
                }
            }

        })
    }
}