package com.aait.cleanapp.GPS

interface GpsTrakerListener {
    fun onTrackerSuccess(lat: Double?, log: Double?)

    fun onStartTracker()
}
